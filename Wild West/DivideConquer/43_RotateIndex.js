/*
    function takes in an input of an array that was previously sorted that has been rotated by an unknown pivot - array is still sorted in rotated state
*/
function findRotatedIndex(arr, num) {
    let left = 0;
    let right = arr.length - 1;

    while (left <= right) {
        let mid = Math.floor((left + right) / 2);

        if (arr[mid] === num) {
            return mid;
        }
        // different vs traditional binary search, takes into account the occurred rotation
        /*

            Handle updating of left and right pointers based on comparison of elements in rotated sorted array -> Adjusts search ranges as binary search occurs

        */
        // check if array portion from left to mid is sorted in increasing order
        // means left portion is a normal sorted subarray
        if (arr[left] <= arr[mid]) {
            // if num is greater than start, but less than mid then it is within the left sorted subarray
            if (num >= arr[left] && num < arr[mid]) {
                // we can adjust the end pointer to be before mid to narrow range
                right = mid - 1;
            } else {
                // else the num has to be in the right sub-array, shift start pointer to after midpoint
                left = mid + 1;
            }
        } else {
            // rotated subarray on left side
            // if num is greater than middle of array and num is less than end (within right subarray)
            if (num > arr[mid] && num <= arr[right]) {
                // adjust start pointer to be after element in middle
                left = mid + 1;
            } else {
                // else then it is in left portion, adjust end pointer to be before middle index
                right = mid - 1;
            }
        }
    }

    return -1;
}
