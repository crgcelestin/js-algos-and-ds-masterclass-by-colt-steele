function sortedFrequency(arr, num) {
    let count = 0
    arr.forEach(ele => {
        if (ele === num) {
            count++
        }
    })
    return count === 0 ? -1 : count
}
