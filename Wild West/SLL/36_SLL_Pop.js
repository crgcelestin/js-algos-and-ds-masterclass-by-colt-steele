class Node {
    constructor(val) {
        this.val = val;
        this.next = null;
    }
}

class SingleLL {
    constructor() {
        this.head = null
        this.tail = null
        this.length = 0
    }
    push(val) {
        if (val === null) return;
        const newNode = new Node(val)
        if (!this.head) {
            this.head = newNode
            this.tail = this.head
        } else {
            this.tail.next = newNode
            this.tail = newNode
        }
        this.length += 1
    }
    pop() {
        if (!this.head) return;
        let current = this.head
        let newtail = current
        if (this.head === this.tail) {
            this.head = null
            this.tail = null
            this.length -= 1
        } else {
            while (current.next) {
                newtail = current
                current = current.next
            }
            this.tail = newtail
            this.tail.next = null
            this.length -= 1
            return current
        }
    }
}
