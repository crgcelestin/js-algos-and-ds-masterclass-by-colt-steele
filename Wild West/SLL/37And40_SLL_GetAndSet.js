class Node {
    constructor(val) {
        this.val = val
        this.next = null;
    }
}

class SinglyLinkedList {
    constructor() {
        this.head = null
        this.tail = null
        this.length = 0
    }
    push(val) {
        var newNode = new Node(val);
        if (!this.head) {
            this.head = newNode;
            this.tail = this.head;
        } else {
            this.tail.next = newNode;
            this.tail = newNode;
        }
        this.length++;

        return this;
    }
    get(index) {
        if (index > this.length - 1) return null;
        if (index < 0) index += this.length;
        let current = this.head
        if (index === 0) {
            return current
        } else {
            while (current && index) {
                current = current.next
                index -= 1;
            }
            return current;
        }
    }
    set(index, val) {
        const current = this.get(index)
        if (current) {
            current.val = val
            return true
        }
        return false
    }
}
