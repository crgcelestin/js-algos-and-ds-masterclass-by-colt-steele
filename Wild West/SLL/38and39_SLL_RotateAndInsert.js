class Node {
    constructor(val) {
        this.val = val;
        this.next = null;
    }
}

class SinglyLinkedList {
    constructor() {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }
    push(val) {
        let newNode = new Node(val);
        if (!this.head) {
            this.head = newNode;
            this.tail = this.head;
        } else {
            this.tail.next = newNode;
            this.tail = newNode;
        }
        this.length++;
        return this;
    }
    rotate(num) {
        num = num % this.length;
        if (num < 0) {
            num = num + this.length;
        }

        while (num > 0) {
            let current = this.head;
            this.head = this.head.next;
            this.tail.next = current;
            this.tail = current;
            current.next = null;
            num--;

        }
        return this;
    }
    unshift(val) {
        const newNode = new Node(val)
        if (!this.head) {
            this.head = newNode
            this.tail = this.head
        } else {
            newNode.next = this.head
            this.head = newNode
        }
    }
    get(idx) {
        if (idx > this.length || -idx > this.length) return null;
        if (idx < 0) idx += this.length
        var current = this.head
        while (idx !== 0) {
            current = current.next
            idx -= 1
        }
        return current
    }
    insert(idx, val) {
        if (idx < 0 || idx > this.length) return false;
        else if (idx === 0) this.unshift(val);
        else if (idx === this.length) this.push(val);
        else {
            let node = new Node(val);
            let prev = this.get(idx - 1);
            node.next = prev.next;
            prev.next = node;
            this.length++;
        }
        return true;
    }
}
