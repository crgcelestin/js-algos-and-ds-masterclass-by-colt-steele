# __Dijkstra's Algorithm__
## Shortest Path Algorithm
- Requires: Graph, Priority Queue (Binary Heap)
- Objectives: Understand significance, implemented weighted graph, steps, implement with native priority Queue, binary heap
- Dijkstra's most commonly works on weighted graphs

### Description
- Famous, widely used algo to find shortest path between graph vertices (fastest from A -> B)
- Uses: GPS (find fastest route), network routing (open shortest path for data), biology (model spread of viruses among humans), airline tickets (cheapest route to destination), and others
- ![Alt text](./images/image.png)
    ^ real world implementation using weighted graph
    * Graph with 6 vertices and 8 edges and weights communicated via respective numbers (4 steps in algorithm)

### Approach
1. Every time node is visited, pick node with smallest known distance to visit first
2. Once we move to next node to visit, look at each neighbor
3. For each neighbor node, calculate the distance by summing total edge leading to node we are checking from starting node
4. If the new total distance to node is < previous total, store new shorter distance for node

![Alt text](./images/image-2.png)
![Alt text](./images/image-4.png)
![Alt text](./images/image-3.png)

* walkthrough
    - Shortest distance from A to A is 0 → 2  neighbors (B, C) A→ B is 4, update B and set to A then A→C is 2, update C to A → C to D is 4 and set D to C as thats how we got them A → C to F is 6 (2+4) and update F to C
    - To get to E from A, A→ B → E which is 7 so updated E to be B
    - Left over nodes [D,E,F], Done [A, C, B]
        - D is smallest at 4, then E
        - D→ E has total of 7 (ignored), then D to F and total is 5 (update 6 to 5) update F to D

        * Update visited to be [A,C,B,D]

        * Left unvisited is E, F

        - Shortest path between E and F is 5 (F), F→ E is 6, while E has 7 so update it to 6
        - Update E to F

    - Done finding shortest path

[Dijkstra's Naive Version](version1.js)
[Dijkstra's Full Version](../DataStructures/Graphs/Dijkstra.js)
1. Fxn accepts starting, ending vertex
2. Create obj (distances) and set each key to be every vertex in the adjacency list w/ value of infinity, except for starting vertex which has 0 value
3. After setting value in distances object, add each vertex with priority of infinity to PQ, except starting vertex, which has a value of 0 as thats the start
4. Create other obj called previous, set each key to be every vertex in adjacency list with value of null
5. Start looping as long there is anything in PQ
    1. Dequeue vertex from priority queue
    2. If vertex is same as ending vertex → done (return)
    3. Else loop through each value in adjacency list of vertex
        1. Calc distance to vertex from starting vertex
        2. If distance is < currently stored in distances object
            1. Update distances object with new lower distance
            2. Update previous object to contain vertex
            3. Enqueue vertex with total distance from start node

(Upgrade w/ PQ) - Improving Dijkstra’s

- Dijkstra’s algo is greedy and can cause problem → improve algo by adding a heuristics (best guess)

(RECAP)

- Graphs = collections of vertices connected by edges, Can be represented using adjacency lists, adjacency matrices, etc. (adjacency list used for this implementation due to simplicity, performance tradeoff)
- Graphs can contain weights, directions, and cycles
- Graphs can be traversed with BFS, DFS
- Shortest path algos like dijkstra can be altered with a heuristic to achieve better results like w/ A*
