/*
    PQ is essential for picking next smallest value an if there are a million nodes it is more difficult, req DS implementation

    - PQ implementation re-sorts every time a new val and priority are enqueued i.e node with weight symbolizing distances

    - Get smallest value out first regardless of initial order

    - Optimize code using min binary heap, PQ implementation below is on a naive level

*/

class PQ {
    constructor() {
        this.values = [];
    }
    enqueue(val, priority) {
        this.values.push({ val, priority });
        this.sort(); //O(n*log(N))
    };
    //returns smallest priority next
    dequeue() {
        return this.values.shift();
    };
    sort() {
        this.values.sort((a, b) => a.priority - b.priority);
    };
}

var pq = new PQ();
pq.enqueue('B', 3);
pq.enqueue('C', 5);
pq.enqueue('D', 2);
pq.enqueue('Q', 20);

/*
     pq.values
[
  { val: 'D', priority: 2 },
  { val: 'B', priority: 3 },
  { val: 'C', priority: 5 },
  { val: 'Q', priority: 20 }
]
*/


export default PQ;
