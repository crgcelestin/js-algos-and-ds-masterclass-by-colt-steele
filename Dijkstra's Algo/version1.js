import PQ from "./simplePQ";
//version 1
var dijkstra = function (start, end) {
    const list = this.adjacencyList;
    const distances = {};
    const previous = {};
    const path = []
    const nodes = new PQ();
    let smallest;
    for (let vertex in list) {
        previous[vertex] = null;
        vertex === start ?
            (distances[vertex] = 0,
                nodes.enqueue(vertex, 0))
            :
            (distances[vertex] = Infinity,
                nodes.enqueue(vertex, Infinity))
    }
    while (nodes.values.length) {
        smallest = nodes.dequeue().val;
        /*
            Before smallest is undefined
            which means there is nothing left in the PQ
            smallest reaches the last node
            can start building the path from start to end
        */
        if (smallest === end) { //build path to return;
            while (previous[smallest]) {
                path.push(smallest);
                smallest = previous[smallest];
            }
            break;
        }
        //look at all neighbors
        /*
            distances[smallest]!==Infinity
            ensures we have visited node,
            therefore look at neighbors
        */
        if (smallest || distances[smallest] !== Infinity) {
            for (let neighbor in list[smallest]) {
                //find neighbor node
                let next = list[smallest][neighbor]
                /*
                 calc distances to neighboring node
                   allows for summing distances from start to end node
                   current distance for node + distance to neighbor
                */
                let candidate = distances[smallest] + next.weight
                let nextNeighbor = next.node
                if (candidate < distances[nextNeighbor]) {
                    //update new smallest distance to neighbor
                    distances[nextNeighbor] = candidate;
                    //update previous - how we got to neighbor
                    previous[nextNeighbor] = smallest;
                    //enqueue in PQ w/ new priority
                    nodes.enqueue(nextNeighbor, candidate);
                }
            }
        }
    }
    //return shortest path with origin and in proper order starting with root
    return path.concat(smallest).reverse();
}
