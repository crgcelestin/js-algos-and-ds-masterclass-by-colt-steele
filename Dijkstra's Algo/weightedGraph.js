class WeightedGraph {
    constructor() {
        this.adjacencyList = {};
    }
    addVertex(name) {
        const list = this.adjacencyList;
        if (list[name]) return 'already addded'
        list[name] = [];
        return `added ${name}:[]`
    }
    addEdge(v1, v2, weight) {
        const list = this.adjacencyList;
        if (list[v1].includes(v2)) return 'edge already exists'
        if (!list[v1]) this.addVertex(v1);
        if (!list[v2]) this.addVertex(v2);
        list[v1].push({ node: v2, weight });
        list[v2].push({ node: v1, weight });
        return `added edge between ${v1} + ${v2}`
    }
}

const graph = new WeightedGraph();
graph.addVertex('A')
graph.addVertex('B')
graph.addVertex('C')
graph.addEdge('A', 'B', 9)
graph.addEdge('A', 'C', 5)
graph.addEdge('B', 'C', 7)

/*
graph.adjacencyList returns ->

{
    A: [{ node: 'B', weight: 9 }, { node: 'C', weight: 5 }];
    B: [{ node: 'A', weight: 9 }, { node: 'C', weight: 7 }];
    C: [{ node: 'A', weight: 5 }, { node: 'B', weight: 7 }];
}

*/
