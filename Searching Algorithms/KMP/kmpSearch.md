# [KMP ALGO](https://www.geeksforgeeks.org/kmp-algorithm-for-pattern-searching/)

## Problem
* Given text txt[0...N-1] and pattern pat[0...M-1] -> write function search(char pat[], char txt[]) printing all occurrences of pat[] in txt[]
* Assume N>M

```js
const txt = 'THIS IS A TEST TEXT'
const pat = 'TEST'
output = 10

const txt2 = "AABAACAADAABAABA"
const pat2 = "AABA"
output= [0,9,12]
```

- Pattern searching is critical as searching for string in notepad/word file, browser/database utilize pattern-searching algos allows fo showing search results

- Time complexity of the KMP algo is O(n+m) worst case vs Naive search pattern search being O(m*(n-m+1))

## KMP (Knuth Morris Pratt) Pattern Searching
* Naive pattern searching algo doesn't work well where there are many matching characters followed by mismatching character

```py
# examples
txt[] = “AAAAAAAAAAAAAAAAAB”, pat[] = “AAAAB”
txt[] = “ABABABCABABABCABABABC”, pat[] =  “ABABAC”
# not a worst case, but a bad case for Naive
```
* KMP matching algo uses degenerating property (pattern has same sub-patterns appearing >1 in pattern) allowing for improvement to O(n+m) worst case
    - when detecting mismatch, know that some characters are in text of next window
* KMP essentially practices optimization where preprocessing is a critical component of this methodology
    - Occurs as we actively pre-process patterns, prepare integer array lps[] indicating # of chars to be skipped

## PreProcessing
- KMP algo preprocess pat[], construct aux lps[] of size m (as pattern size) allowing for character skipping
- Name lps indicate longest proper prefix and suffix
    * Proper prefixes of 'ABC' are '','A', 'AB'
    * search for lps in subpatterns i.e sub-strings of patterns are both prefix, suffix

### Pseudcode
1. Calculate values in lps[] by keeping track of length of longest prefix suffix value for previous index
2. initialize lps[0] and len as 0
3. if pat[len] and pat[i] match, increment len by 1 and assign incremented value to lps[i]
4. if pat[i] and pat[len] don't match and len is not 0, update len to lps[len-1]

## Implementation of KMP algo
1. Start comparison of pat[j] with j=0 with chars of current text window
2. keep matching chars of text[i] and pat[j], keep incrementing i and j while pat[j] and txt[i] keep matching
3. when a mismatch is found:
    1. know that chars pat[0...j-1] = txt[i-j...i-1]
    2. know that lps[j-1] == len(pat[0...j-1])
    3. from above 2 points, don't need to match these lps[j-1] chars with txt[i-j...i-1] as characters will have to match anyway
