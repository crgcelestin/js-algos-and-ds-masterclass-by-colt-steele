// preprocessing for kmp algo
function computeLPS(pat, M, lps) {
    // length of prev longest prefix suffix
    var len = 0;
    var i = 1;
    lps[0] = 0;
    // calculate lps[i] for i= (1...M-1)
    // lps is array holding the longest proper prefix, suffix
    while (i < M) {
        if (pat.charAt(i) == pat.charAt(len)) {
            len++;
            lps[i] = len;
            i++
        }
        // encounter mismatch and len is greater than 0, skip to non-mismatch state
        else {
            if (len != 0) {
                len = lps[len - 1]
            } else {
                lps[i] = len;
                i++;
            }
        }
    }
}
function KMP_Search(pat, txt) {
    // sub-string of interest
    var M = pat.length
    // entire input string
    var N = txt.length

    // lps[] holds longest string of characters, prefix + suffix values for pattern
    var lps = []
    var j = 0 // index for pat[]

    // preprocess pattern i.e. calculate lps[]
    computeLPS(pat, M, lps)

    var i = 0;
    while ((N - i) >= (M - j)) {
        if (pat.charAt(j) == txt.charAt(i)) {
            j++
            i++
        }
        if (j == M) {
            document.write("Found pattern " + "at index " + (i - j) + "\n");
            j = lps[j - 1];
        }
        // mismatch following j matches
        else if (
            i < N && pat.charAt(j) != txt.charAt(i)
        ) {
            // lps[0...lps[j-1]] match regardless
            if (j != 0) {
                j = lps[j - 1]
            } else {
                i = i + 1
            }
        }
    }
}
