function binarySearch(arr, value) {
    let left = 0
    let right = arr.length - 1
    let middle;
    while (left <= right) {
        let middle = Math.floor((left + right) / 2)
        if (arr[middle] === value) return middle;
        else if (arr[middle] < value) left = middle + 1;
        else right = middle - 1;
    }
    return arr[middle] === value ? middle : -1
}

/*
Worst case - O(log n)
    2^steps=elements
    log(16 - base 2) = 4
    16 elements, 4 steps
    when u double arr length, add 1 extra step to find target value
best case - O(1)
    middle of array = target value then its one operation
*/
