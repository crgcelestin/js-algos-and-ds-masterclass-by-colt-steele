# Searching Algo Notes

## Intro to Searching
   ```javascript
    //built in search for js w/ .indexOf
    var usernames=["tommy", "i_hate_cats", "last_username"]
    usernames.indexOf('i_hate_cats')
    //1
    usernames.indexOf('i_love_cats')
    //-1

    /*
        example of using indices to check if new profile to be created with user desired username exists in db, if it does return err else create username
    */
    x=user.input()
    if usernames.indexOf(x)>-1:
        return 'name is taken'
    else:
        try{
            newProfile=new profile(x)
            db.create(newProfile)
            db.save()
        } catch(e){console.error(e)}
   ```
* Objectives
    - Describe what a search algo is, implement linear search, binary search, naive string searching algo, kmp string searching algo

## How Do We Search?
* Simplest method
    -   Given array, simplest way to search for value = look at every array element, check if value we want exists
    - Linear search: .indexOf, includes, find, findIndex → check every element one at a time

* Binary Search
    - Faster form of search → as opposed to element elimination one at a time (Linear Search), eliminate half of elements at a time and only works on sorted arrays (Binary Search)

* Native String Search
    - Want to count number a times a smaller string appears in longer string, straightforward - check pairs of chars indv
    - omg → wowomgzomg (2 matches)

    - __Pseudocode__:
     * (0) function called stringSearch(str1, str2)
     * (1) loop over longer string
     * (2) loop over shorter string
     * (3) if chars don’t match break out of inner loop
     * (4) if chars do match, keep going
     * (5) complete inner loop and find match, increment match count
     * (6) return count

* KMP String Search
    - Knutt-Morris-Pratt algo offers improvement over naive approach, published in 1977 → more intelligently traverses long string → reduce amt of redudant searching
    - How does KMP know how far to traverse?
    -![kmp](Images/kmp.png)
    - Prefixes+Suffixes:
        * (1) To determine how far to shift shorter string, pre-compute length of longest (proper) suffix matching a (proper) prefix
        * (2) Tabulation occurs prior one starts to look for short string in long string

* Core Concepts
    - Searching is a common task
    - Linear Search is best for unsorted collection
    - Binary Search is best for a sorted collection
        - So with binary, implement the most optimal sorting algo then perform binary as opposed to linear search
    - String Search (naive) is O(n*m), with n being the string of desired searching and m being the string searched
    - KMP is a linear time searching algo for string searches arriving at a Big O of O(n+m)
