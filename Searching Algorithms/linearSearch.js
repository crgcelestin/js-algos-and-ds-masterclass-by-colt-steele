function linearSearch(arr, value) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === value) {
            return i
        }
    }
    return -1;
}

//O(n) time complexity
//best case: O(1) we find desired target at start
//worse case: O(n) target is at end of array

//alt linear search using multiple pointers
function linearSearch(arr, value) {
    let left = 0
    let right = arr.length - 1
    while (left <= right) {
        if (arr[left] === value) return left;
        else if (arr[right] === value) return right;
        else {
            left++
            right--
        }
    }
    return -1;
}
