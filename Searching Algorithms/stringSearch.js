function stringSearch(str, substr) {
    let count = 0;
    for (let i = 0; i < str.length; i++) {
        let j = 0;
        let isMatch = true;
        while (j < substr.length) {
            if (substr[j] !== str[i + j]) {
                isMatch = false;
                break;
            }
            j++;
        }
        if (isMatch) {
            count += 1;
        }
    }
    return count ? true : false
}

/*
    Tc is O(n*m), sc is O(1) as only a single var with count is being returned or changed/established
*/

//code with psuedocode included
const stringSearch2 = (str, subStr) => {
    //initialize count
    let count = 0;
    //loop over longerstring
    for (var i = 0; i < str.length; i++) {
        //set a var = true boolean, use that to trak substr matches
        let isMatch = true;
        //iterate through substring
        for (var j = 0; j < subStr.length; j++) {
            /*
            now we are iterating through both the long string and substring
            as we iterate through long string
            if we don't find a match starting with the 0th
            index of the substring in string
            we set isMatch to false and break out of loop
            but if when we iterate through the long string
            and if we find a match with the str and substring
            isMatch stays true and we add 1 to count
            */
            // i+j looking ahead, ensure that substring is
            // a certain match in first string
            if (subStr[j] !== str[i + j]) {
                isMatch = false;
                break;
            }
        }
        //find match and increment count
        if (isMatch) {
            count++;
        }
    }
    //return count, number of substring matches in str
    return count;
}

/**
 * @param {string} w
 * @param {string} t
 * @return {boolean}
 */

var string_search_naive = function (w, t) {
    const main_str = w.toLowerCase().split(" ")
    console.log(main_str)
    const dict = {}
    for (const char of main_str) {
        dict[char] = (dict[char] || 0) + 1;
    }
    return dict[t] ? true : false
}

console.log(stringSearch(
    'i built a nest and test it',
    'built',
))

console.log(string_search_naive(
    'i built a nest and test it',
    'built',
))
