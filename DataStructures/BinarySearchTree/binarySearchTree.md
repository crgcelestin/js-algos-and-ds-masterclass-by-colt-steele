# Binary Search Trees
- Define Tree, Tree v List, Differences between various trees,Implement Operations on BSTs

## Summary
- A Tree is a Data Structure consisting of nodes in parent-child relationship, each node can have pointers that refer to one or more other nodes
- Lists are linear as opposed to trees which are nonlinear as trees involve various paths
- Singly Linked List is a special case of a tree
- With trees, nodes can only point to children and there can only be one root vs graphs which can be cyclic

### Tree Terminology
- Root is top node in tree
- Child is a node that directly connects to another node when moving away from root
- Parent is the converse notion of a child (Node that a child points to)
- Siblings are groups of nodes with same parent
- A Leaf is a node with no children
- Edge is the actual connection between nodes

__Uses of Trees__
- There are several real world applications:
    - HTML DOM includes a tree like structure (parent child relationship with text)
    - Network routing, abstract syntax trees, artificial intelligence (breakdown logic of game, moves into a tree allowing ai to understand best potential options),
    - Folders in operating system, JSON (js object notation, parse string into python there is code that traverses a tree like structure)

### Binary Search Trees

[BST Implementation](BST.js)

[Additional BST Methods](OtherBSTMethods.js)

- Binary Search Trees excel at searching in order to perform data retrieval
- Trees that have 0-2 children and are sorted in particular order through strings, numbers and enable comparisons
- Every node (child) with ‘value less than parent’ are located to left of parent and nodes (children) with ‘value greater than parent’ are located to right of parent

- Searching
    - BST structure allows for easy insertion and searching
    - If looking for number x or inserting, making a comparison allows is to chop a tree/tree traverse by TC: 1/2 O(log N) depending on length of tree of N depths

- BST Big O
    - Insertion
        - If tree size is doubled, has only 1 more step
        +1 in terms of tree height
        - 2x number of nodes: 1 step,
        - 4x: 2 steps,
        - 8x: 3 steps

### BST Traversal
[BST Traversal](BSTtraversal.js)
- Imagine methods with a tree that looks like this:
![BST Image](BST.png)

- #### BFS (Breadth First Search)
    - Involves going across tree as is shown in the image with arrows
    - Pushing each node visited into Queue
         - (1) Starting with root -> (2) Left to right on successive heights towards tre bottom -> (3) Until leaf nodes -> (4) Return Queue
        - ! BFS implemented in Tree Traversal File is Recursive !
        - ! Non Recursive BFS Implementation !
            ```javascript
                bfs(){
                  var node=this.root,
                  data=[],
                  queue=[];
                  queue.push(node);
                  while(queue.length){
                    node=queue.shift();
                    data.push(node.value);
                    if(node.right) queue.push(node.left);
                    if(node.left) queue.push(node.right);
                  }
                  return data;
                }
            /*
            However, the first implementation using recursion has a space complexity of O(h),
            where h is the height of the binary tree, because it uses the call stack to keep track of the nodes.
            In the worst case scenario, when the binary tree is skewed and has a height of n, the space complexity will be O(n).
            On the other hand, the second implementation using a while loop and a queue has a space complexity of O(w), where w is the maximum width of the binary tree,
            because it uses a queue to keep track of the nodes.
            In the worst case scenario, when the binary tree is a complete binary tree and has all its levels filled except possibly the last one, the width of the tree will be 2^(h-1),
            where h is the height of the tree, and the space complexity will be O(2^(h-1)).

            Therefore, the second implementation using a queue is generally preferred because it has a lower worst-case space complexity, especially for skewed binary trees.
            */
            ```

- #### DFS (Depth First Search)
    - Three different methods as it pertains
    - All methods showing output with given BST
    ![BST](BST.png)
        - Assuming at each step, pushing nodes into Queue
        - (1) InOrder: Outputs [3,6,8,10,15,20]
        - Basically the tree flattened - all nodes in underlying order
            - (1) Start with minimum of Left Branch (min leaf)
                - [3]
            - (2) Traverse from min to max values in Left Branch
                -[3->6->8]
            - (3) After exhausting Left Branch, move to Root
                - [10]
            - (4) Move to Right Branch after Root
            - (5) Start with min on Right Branch
                - [15]
            - (6) Traverse from min to max values in Right Branch
                - [15->20]
            - (7) After Right Branch max, return nodes visited
        - (2) PreOrder: Outputs [10,6,3,8,15,20]
        - Exported tree structure that is easily reconstructed/copied
        - Root -> left side -> exhaust left side -> next right side -> exhaust right side -> ... -> exhaust right side -> max right node
            - (1) Start from Root Node
                - [10]
            - (2) After root, traverse left branch continuing on the left side of the left branch towards min leaf
                - [10->6]
            - (3) After min leaf, traverse right branch of left branch of root
                - (Not in this scenario) Continue pattern with other branches on left branch continuing towards the right
            - (4) Continue same pattern after exhausting left branches of root then exhaust right branches of root
                -  Traverse right branch from left to right (exhausting left branch first then right successively)
        - (3) PostOrder: Outputs [3,8,6,20,15,10]
            - (1) Start with min leaf of tree
            - (2) Exhaust left branch in successive height (bottom -> top) order then exhaust left branch
                - [3->8->6]
            - (3) Move on to right branch starting with min leaf then exhaust right branch
                - [20->15]
            - (4) After exhausting left and right branch, visit Root
                - [10]

- ### Choosing BFS v DFS
* ![choose DFS](../../Images/chooseDFS.png)
    - DFS
        - DFS is good for:
            - For trees that are wide (several left and right branches) and are quite shallow (smaller heights)
                - Wide, short trees
            - DFS is concerned about individual branch depth vs BFS which is concerned about horizontal depth
        - DFS is not good for:
            -  Bad complexity as it relates to deep and skinny trees as it requires keeping other nodes in memory for deep vertical trees
* ![choose BFS](../../Images/chooseBFS.png)
    - BFS
        - BFS is good for:
            - For trees that are deep (massive heights) and don't have many branches (thin)
            - Thin, high trees
        - BFS is not good for:
            - wide short height trees
            -  If there is a massive tree with several nodes to track, space for BFS has bad space complexity vs DFS, as BFS requires heavy storage req v DFS where one keeps track of nodes in current branch

## RECAP
-  Trees are non linear DS with root, child nodes, parent nodes, leafs, edges
- Binary trees can have values of any type but at most 2 children per parent
- BSTs are a specific version of Binary Trees where every node to left of parent is less than curren value and node to right is greater than current value
- Can search through BSTs using BFS or DFS (InOrder, PreOrder, PostOrder)
