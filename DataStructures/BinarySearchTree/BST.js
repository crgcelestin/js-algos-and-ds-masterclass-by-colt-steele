class Node {
    constructor(value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }
}

class Queue {
    constructor() {
        this.first = null;
        this.last = null;
        this.size = 0;
    }
    //add to end of queue - last out, last in
    //											first out, first in
    //push
    enqueue(val) {
        var newNode = new Node(val);
        if (!this.size) {
            this.first = newNode
            this.last = newNode
        } else {
            this.last.next = newNode
            this.last = newNode
        }
        this.size += 1
        return this
    }
    //removing from start of q - first in, first out
    //                           last in, last out
    //shift
    dequeue() {
        if (!this.size) return;
        var temp = this.first;
        if (this.size == 1) {
            this.last = null;
        }
        this.first = this.first.next;
        this.size--;
        return temp.value;
    }
}

class BST {
    constructor() {
        this.root = null;
    }
    insert(val) {
        var newNode = new Node(val)
        if (!this.root) {
            this.root = newNode
            return this
        }
        const inserted = (val, current = this.root) => {
            if (val === current.value) {
                current.count += 1
                return this
            }
            if (val > current.value) {
                if (current.right) {
                    return inserted(val, current.right)
                } else current.right = newNode;
            } else {
                if (current.left) {
                    return inserted(val, current.left)
                } else current.left = newNode;
            }
            return this;
        }
        return inserted(val)
    }
    search(val) {
        if (!this.root) {
            return
        }
        const searched = (val, current = this.root) => {
            if (val === current.value) {
                return current
            }
            if (val > current.value) {
                if (current.right) return searched(val, current.right);
            } else {
                if (current.left) return searched(val, current.left);
            }
        }
        return searched(val)
    }
    delete(val) {
        if (!this.root) return;
        const deleteNode = (val, curr) => {
            if (!curr) return null;
            if (val < curr.value) {
                curr.left = deleteNode(val, curr.left);
            } else if (val > curr.value) {
                curr.right = deleteNode(val, curr.right);
            } else {
                // val is equal to current node's value

                //if that node has duplicates, decrement
                if (curr.count > 1) {
                    curr.count--;
                    return curr;
                    //if node has no children
                } else if (!curr.left && !curr.right) {
                    return null;
                    //if node has 1 child
                } else if (!curr.left || !curr.right) {
                    const child = curr.left || curr.right;
                    return child;
                } else {
                    // 2 children, find min value of right subtree
                    let minR = curr.right;
                    while (minR.left) {
                        minR = minR.left;
                    }
                    curr.value = minR.value;
                    curr.count = minR.count;
                    curr.right = deleteNode(minR.value, curr.right);
                }
            }

            return curr;
        }
        this.root = deleteNode(val, this.root);
        return this;
    }
    /*
        BFS Pseudocode
        (1) create q and var to store values of nodes visited
        (2) place root node in q
        (3) loop as long as queue exists w/ elements which
            means that there are still nodes in binary search tree
            (a) deq node from q
            (b) push value of node into var that stores nodes
    */
    bfs() {
        const queue = new Queue()
        const nodes = []
        if (!this.root) return nodes;
        var current = this.root
        queue.enqueue(current)
        const bfs_traverse = (queue) => {
            if (!queue.size) return nodes;
            const currentNode = queue.dequeue();
            nodes.push(currentNode.value);
            if (currentNode.left) queue.enqueue(currentNode.left);
            if (currentNode.right) queue.enqueue(currentNode.right);
            return bfs_traverse(queue);
        };
        bfs_traverse(queue)
        return nodes
    }
    // dfs methods
    // recursive format
    dfs_inorder() {
        var nodes = []
        var current = this.root
        const helper = (current) => {
            if (current.left) helper(current.left);
            nodes.push(current.value);
            if (current.right) helper(current.right);
        }
        helper(current)
        return nodes
    }
    dfs_preorder() {
        var nodes = []
        var current = this.root
        const helper = (current) => {
            nodes.push(current.value)
            if (current.left) helper(current.left)
            if (current.right) helper(current.right)
        }
        helper(current)
        return nodes
    }
    dfs_postorder() {
        var nodes = []
        var current = this.root
        const helper = (current) => {
            if (!current) return nodes;
            if (current.left) helper(current.left);
            if (current.right) helper(current.right);
            nodes.push(current.value);
        }
        helper(current)
        return nodes
    }
}

let tree = new BST()
tree.root = new Node(10);
tree.root.right = new Node(15);
tree.root.left = new Node(8)
tree.root.left.right = new Node(9)
