# 10.1
[AVL Tree - Insertion and Rotations](https://www.youtube.com/watch?v=jDM6_TnYIqE)

## Video Notes
- (1) BST Description
    - Tree with root node (7 nodes, 3 right, 3 left, 1 root), all nodes to right of root are greater and left are lesser and this pattern removes with lower depths
    - Searching in BST depends on height of tree from root to leaves: best case is log(n) and worst case is n
    - Height is determined by order of key insertion that further dictates height and lookup efficiency

- (2) BST Drawbacks
    - Tree shown has height of log(n)
        ![1ST BST](../../../Images/1st%20BST.png)
        * Major gripe with BSTs is that there is no insertion of duplicates

    - Tree has height of n with same elements
        ![2ND BST](../../../Images/2nd%20BST.png)
        * Viewing both examples, there is a discrepancy regarding heights due to node insertion order
    - Height is not an element of control, dependent on element order
    - As searching is done in a linear order, the time taken for search operation is proportional to the height of the tree

- (3) How to improve BST
    - For same set of keys, getting different orders with 3 keys, there are 3! meaning 6 potential permutations
    - Can perform rotations to achieve optimal tree regarding keys, order of insertion to convert to balanced binary search tree
    ![Rotations](../../../Images/Rotations.png)
    - Rotations are only done on 3 nodes at a time

- (4) What is an AVL Tree ?
    - Height balanced BST where a balance factor is determined by height of left subtree - height of right subtree
    - bf = h(l)-h(r)={-1,0,1}
    - |bf| = |h(l) - h(r)|<=1
        - if a bst is imbalanced, perform rotations in order to convert to avl tree
        ![BandUb](../../../Images/balanced+unbalanced.png)
        - 1st tree is balanced, last two are unbalanced due to the respective heights of root and leaf nodes
    - In order to determine balancing process
        1. Perform insertion of keys
        2. Interpret the imbalance of respective nodes in BST
        3. Determine where the rotation must occur among 3 nodes [ RL, LL, LR, RR Imbalance Rotation ]
        4. After rotation imbalance must be within lower, upper limits
        - Single step LL Rotation
            ![LL Rotation](../../../Images/LLRotation.png)
        - Double step LR Rotation
            ![LR Rotation](../../../Images/LRRotation.png)
        - Single step RR Rotation
            ![RR Rotation](../../../Images/RRRotation.png)
        - Double Step RL Rotation
            ![RR Rotation](../../../Images/RLRotation.png)

- (5) ALV Tree (Creation)
    - Given a BST with key insertion involving [ 40, 20, 10, 25, 30, 22, 50 ]
        - Create AVL Tree given keys for bst creation
            1. Insert first three then perform rotation given imbalance of root node
            ![1st Node Triplet](../../../Images/1stThreeNodes.png)
                - LL Rotation
            2. Second three nodes group
            ![2nd Node Triplet](../../../Images/2ndThreeNodes.png)
                - LR Rotation
            3. Perform 2 rotation types in order to get keys to final balanced AVL Tree
            ![Last](../../../Images/LastFewNodes.png)
                - Perform a RL then RR Rotation to get to final balanced state
