class Node {
  constructor(value) {
    this.value = value;
    this.left = null;
    this.right = null;
  }
}
class BST {
  constructor() {
    this.root = null;
  }
  getHeight() {
    const getHeightRec = (node) => {
      if (!node) {
        return 0;
      }
      const leftHeight = getHeightRec(node.left);
      const rightHeight = getHeightRec(node.right);
      return 1 + Math.max(leftHeight, rightHeight);
    };
    return getHeightRec(this.root);
  }
  getNodes() {
    const nodes = [];
    const traverse = (curr) => {
      if (curr === null) {
        return;
      }
      nodes.push(curr.value);
      traverse(curr.left);
      traverse(curr.right);
    };
    traverse(this.root);
    return nodes;
  }
  getmin() {
    if (!this.root) {
      return null
    }
    let current = this.root
    while (current.left) {
      current = current.left
    }
    return current.value
  }
  getmax() {
    if (!this.root) {
      return null
    }
    let current = this.root
    while (current.right) {
      current = current.right
    }
    return current.value
  }
}
