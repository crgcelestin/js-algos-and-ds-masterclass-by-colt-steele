# 10.2
__[B Trees and B+ Trees. How they are useful in Databases](https://www.youtube.com/watch?v=aZjYr87r1b8)__

## Disk Structure
- Disk contains logical circles that are known as 'tracks' that are mini circles running on disk, sections are areas on disk
    ![Disks](../../../Images/DiskStructure.png)
        - Each block address is dictated by the combination of track and sector number
    - In each block there are bites, defined by offsets in block that defines own address
        - Bite = [track][sector][offset] -> Bite=[block][offset]

[Read About the Rest of B Trees on GeeksforGeeks for the sake of time](https://www.geeksforgeeks.org/introduction-of-b-tree-2/#)

## B Tree Implementation
- [B Tree JS](bTree.js)
- [B Tree Py](bTree.py)

## Description
- Traditional binary search trees are limited, B-Trees serve as a multi-talented ds that is able to handle large amt of data
    - BSTs suffer with storing + searching due to poor performance, high memory usage
    - B Trees are self balancing and are designed to overcome limitations of poor performance of bsts

### B-Tree Structure
- B trees are characterized by large # of keys that store a single node i.e 'large key' trees
- Each node in B tree can store several keys allowing tree to have larger branching factor and shallower height
    - shallow height = less disk i/o resulting in faster search and insertion operations
    - well suited for storage systems with slow, bulk data access
- B trees maintain balance with lower bound of key quantity
    - Guarantees tc for insertion, deletion, searching is O(log n)

### B-Tree Properties
- All leaves @ same level
- B Tree is defined by a min degree 't' dependent on disk block size
- Each node except root has t-1 keys (root contains min 1 key)
- all nodes contain at most [2t-1] keys
- Number of children @ node = keys + 1
- All node keys are sorted in increasing order (child btwn k1, k2 has all keys in range k1, k2)
- B tree grows, shrinks from root unlike BST
- Insertion of node in b tree only happens at leaves
* B-Tree Example
    - ![b-tree](b-tree.png)
    - All leaf nodes are at same level, non-leafs have no empty sub-tree, have keys children-1

### B Tree Traversal
* similar to inorder traversal of binary tree
    1. start from leftmost child
    2. recursively print from leftmost
    3. repeat process for remaining children and keys
    4. recursively print rightmost child
    ```js
    // inorder traversal
    helper
        if current.left, helper(current.left)
        print(current.value)
        if current.right, helper(current.right)
    ```

### Search Operation in B-Tree
- search is similar to search in binary search tree
    1. start from root, recursively traverse down
    2. for every visited non leaf node
        1. if node has key, return node
        2. else, recur down to appropriate child of node
    3. if we reach leaf node, don't find k in leaf node -> return NULL
    ```js
    // search involves knowing that children of node in binary tree, if greater than parent are in right node, if less then left node
    search(val)
        current=this.root
        if val===current.val, return current
        else if val greater than current.val, recursively perform search with val, current.right
        else if val less than current.val, recursively perform search with val, current.left
    ```

### B-Tree Example
- ![Search B-Tree](search.png)
    1. search for 120 in provided tree
    2. start with root node
    3. check in which range key is, 120>100 and <130 so in left side of the right branch
- Search is predicated off the concept of limiting the changes where key containing a value can be present

## Finish Implementation
- [Insertion](https://www.geeksforgeeks.org/b-tree-set-1-insert-2/)
- [Deletion](https://www.geeksforgeeks.org/b-tree-set-3delete/)
