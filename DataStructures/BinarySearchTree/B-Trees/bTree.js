// all nodes at most contain 2t-1 keys
// each node except root has t-1 keys
class Node {
    constructor(t, leaf) {
        this.t = t;
        this.leaf = leaf;
        this.keys = new Array(2 * t - 1);
        this.C = new Array(2 * t);
        this.n = 0;
    }
    traverse() {
        for (let i = 0; i < this.n; i++) {
            if (this.leaf == false) {
                C[i].traverse()
            }
            document.write(keys[i] + "")
        }
        if (leaf == false) {
            C[i].traverse();
        }
    }
    search(val) {
        let i = 0;
        while (i < n && val > keys[i]) {
            i++;
        }
        if (keys[i] == k)
            return this;
        if (leaf == true)
            return null;
        return C[i].search(val)
    }
}

// t = min degree that defines a B-Tree
class BTree {
    constructor(t) {
        this.root = null;
        this.t = t;
    }
    traverse() {
        if (this.root != null)
            this.root.traverse();
        document.write('<br>')
    }
    search(val) {
        if (this.root == null)
            return null;
        else
            return this.root.search(val);
    }
}
