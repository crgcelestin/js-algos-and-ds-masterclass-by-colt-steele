# __PRIORITY Q__
- Section: Data Structure where each element (node) has a relative priority and elements with higher priorities are served prior to elements with lower priorities
    - [max bin heap](#max-binary-heaps)

# PQs - Similar to BST
- Example of Priority Q
    * [p:5, p:1, p:2, p:5, p:4] - iterate to find highest priority element which is with lower numbers involving O(n) comparisons
    * Heaps are used for an efficient priority queue
        - Insert item with higher priority (bubble up), item with lower priority (bubble down)
        - High priority gets served akin extractMax() from heap

- Min PQ (Binary Heap where low priority means higher priority)
    - Enqueue
        * (a) accept node with value, priority attributes
        * (b) make new node
        * (c) using bubbleUp put it in right spot based on priority
    - Dequeue
        * (a) remove root element
        * (b) return root -> rearrange heap using priority

- Time Complexity
    - Space complexity is O(log n) for insertion and removal
    - Converting this to max bin heap involves switching of comparison operators
