# __BIN HEAP__
- Section: Define what a bin heap is, min v max heaps, implement basic heap methods, understand where heaps are used in real world and what other data structures can be constructed from heaps
    - [max bin heap](#max-binary-heaps)
    - [storing heaps](#storing-heaps)
    - [insert val](#insertval)
    - [extract max](#extractmax)
    - [big o of bin heaps](#big-o-of-bin-heaps)
    - [heapsort](#heapsort)

## Intro
> - Heaps are a different type of tree and are similar to BSTs
>   - However with BSTs there is no order inherently with left and right branches
> - maxBinary Heaps have parents that are larger than child nodes
> - minBinary Heaps have parent nodes that are smaller than children

## Max Binary Heaps
- Each parent has at most 2 children with value of each parent node is always greater than child nodes
    - in maxBinary Heaps the parent is greater than children with no guarantees between sibling nodes
- Binary Heap structure: Compact as possible, all children of each node are full as can be and left children are filled first
    - Max Bin Heap
        ![maxBin](../../Images/maxBinHeap.png)
    - Min Bin Heap
        ![minBin](../../Images/minBinHeap.jpeg)
            - Smallest Nodes are at top of binary heap, being the reverse of max bin heap

## Storing Heaps
- It makes the most sense to use arrays with bin heap (min, max) vs full Node structure and involving the updating of pointers (.left, .right)
    -   As binary heaps are typically balanced, there is never a hole in the array
    - It takes less space as an array is more memory efficient when compared to nodes
    - As heaps are designed with formulas for parent and child nodes, it is easier to abstract array as BST using heaps
- TLDR: BinHeap array allows for savings on memory overhead, increased speed as a result of data locality [ contiguous memory = caching ]

> - Easy method of storing bin heap is via list/array
>   - __For any index of array n, left child is stored at 2n+1, right child is at 2n+2 (example: parent is at 4th index, left child is at index 9 and right is at index 10)__
>   - __For any child at index n, parent is at index Math.floor((n-1)/2) (example: child is at index 9 -> Math.floor((9-1)/2)=4, child is at index 10, Math.floor((10-1)/2)=4)__

# Binary Heap Methods
## Insert(val)
- Adding to MaxBinary Heap involves BinHeap.push(new_val) -> bubble new value up via comparisons to parent
    - i.e arr[(n-1)/2] vs arr[2n+1], arr[2n+2]

- __Adding New Value to Bin Heap__
        ![Add](../../Images/addtoBinHeap.png)
- __Propagation/Bubble Up__
        ![Bubble](../../Images/BubbleUp.png)

* __Pseudocode__
    * (a) Insert value into values property i.e. array of 'nodes' via this.values.push(val)
    * (b) bubble up process
        - (i)
        set var 'nodes' = values array
        parent Index = formula that points to parent element `((n-1)/2)`
        child element is the current element which is the element being pushed in
        parent element is nodes[parent Index]
        - (ii)
        loop as long as value of element at parentIndex is less than element at child index and current index is not root node
            1. swap element at parent Index with element at child Index (const swap)
            2. after swapping, set indices to correct elements
    * (c) return list of now binHeap ordered nodes


## ExtractMax()
- For max Binary Heap, remove max element being the root, for min heap it involves min element -> Priority Q implementation (each element is assigned a priority level, start with root in maxBinary Heap)
    - Similar to insert where one can't just remove node and must swap last value and adjust (sinking down)
    - Sink Down (Bubble Down): Delete root from heap [ extract max element in max, min ele in min-heap ] and restoring properties i.e preserve heap structure

- __Remove and Swap__
    ![Pt 1](../../Images/RemoveSwapPt1.png)
    ![Pt 2](../../Images/RemoveSwapPt2.png)
- __Sinking Down__
    ![Pt1](../../Images/SinkDownPt1.png)
    ![Pt2](../../Images/SinkDownPt2.png)
    ![Pt3](../../Images/SinkDownPt3.png)

* __Pseudocode__
    * (a) parent Index is set to default 0, with max being default root node
    * (b) if bin heap is empty return, if only 1 element then pop node and return it
    * (c) invoke swap function with array elements, 0th index (root), and last element in values array
    * (d) pop as now max is last node in bin heap
    * (e) bubble down node that was swapped until in correct position for heap structure
    * (f) return popped max element

## BIG O OF BIN HEAPS
- Min and Max are great for insertion and deletion with with Big O(log N) and as N grows ability to insert and remove grows at a slower pace

- The worst case scenario for max number of insertions
    - E.g. We desire the insertion of 200 in a tree whos max node is 100, have to compare it 1 at a time per row/level

- If there are 16 elements in a bin heap, then there are a series of 4 comparisons, 2^n (32 nodes = 5 comparisons)

- Bin Heaps are a useful data structure for sort, implementing Data structures like priority queues - they are either max or min Binary Heaps with parents being either smaller or larger than children
    - Math allows heap representation as arrays
    ![Skinny](../../Images/SkinnyBinaryTree.png)

## HEAPSORT
- ( Can sort an array in O(n log n) time and O(1) space by making it a heap)
    1. Make array a max heap using maxHeapify
    2. Loop over array and swap root node with last item in array
    3. After performing a swap, run maxHeapify to find next root node
    4. Next loop swap root node with second to last item in array and run maxHeapify
    5. After running out of items to swap, array is sorted
