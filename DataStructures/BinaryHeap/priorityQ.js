class Node {
    constructor(val, priority) {
        this.value = val
        this.priority = priority
        // can implement 3rd parameter with time
        // compare priorities and time stamps
        this.insertTime = Date
    }
}
class PriorityQueue {
    constructor() {
        this.values = []
    }
    swap(arr, idx1, idx2) {
        [arr[idx1], arr[idx2]] = [arr[idx2], arr[idx1]]
    }
    bubbleUp(cIndex) {
        let nodes = this.values
        let pIndex = Math.floor((cIndex - 1) / 2)
        let child = nodes[cIndex]
        let parent = nodes[pIndex]
        while (cIndex > 0 && child.priority <= parent.priority) {
            this.swap(nodes, cIndex, pIndex)
            cIndex = pIndex
            pIndex = Math.floor((cIndex - 1) / 2)
            child = nodes[cIndex]
            parent = nodes[pIndex]
        }
    }
    enqueue(val, priority) {
        const node = new Node(val, priority)
        this.values.push(node)
        this.bubbleUp(this.values.length - 1)
        return this
    }
    bubbleDown(pIdx) {
        let nodes = this.values
        let cLIdx = 2 * pIdx + 1
        let cRIdx = 2 * pIdx + 2
        let Idx2swap = null
        let len = this.values.length - 1
        // if the index of the left child is within upper range of length and the priority of left is less than that of right
        if (cLIdx <= len && nodes[cLIdx].priority < nodes[pIdx].priority) {
            Idx2swap = cLIdx
        }
        // if index of right child is within upper range and priority of right child is less than left child priority or parent index
        if (cRIdx <= len && nodes[cRIdx].priority < nodes[cLIdx || pIdx].priority) {
            swap = cRIdx
        }
        if (Idx2swap && nodes[Idx2swap].priority < nodes[pIdx].priority) {
            this.swap(nodes, Idx2swap, pIdx)
            this.bubbleDown(Idx2swap)
        }
    }
    dequeue() {
        if (!this.values) return;
        if (this.values.length <= 1) {
            let last = this.values.pop();
            this.values = []
            return last
        }
        this.swap(this.values, 0, this.values - 1);
        const ele = this.values.pop();
        this.bubbleDown(0)
        return ele
    }

}
let PQ = new PriorityQueue()
PQ.enqueue('gunshot', 0)
PQ.enqueue('fever', 1)
PQ.enqueue('common cold', 2)
