class MaxBinHeap {
    constructor() {
        this.values = [41, 39, 33, 18, 27, 12]
    }
    swap(arr, idx1, idx2) {
        return [arr[idx1], arr[idx2]] = [arr[idx2], arr[idx1]]
    }
    // move larger values to root node i.e to parent Indices
    bubbleUp(cIndex) {
        let nodes = this.values
        let pIndex = Math.floor((cIndex - 1) / 2)
        let child = nodes[cIndex]
        let parent = nodes[pIndex]
        while (cIndex > 0 && child > parent) {
            this.swap(nodes, cIndex, pIndex)
            cIndex = pIndex
            pIndex = Math.floor((cIndex - 1) / 2)
            child = nodes[cIndex]
            parent = nodes[pIndex]
        }
    };
    insert(val) {
        this.values.push(val)
        this.bubbleUp(this.values.length - 1)
        return this;
    }
    bubbleDown(pIdx) {
        let nodes = this.values
        // math reps of left child = 2n+1
        let cLIdx = 2 * pIdx + 1
        // math reps of right child = 2n+2
        let cRIdx = 2 * pIdx + 2
        let Idx2swap = null;
        let len = this.values.length - 1
        if (cLIdx <= len && nodes[cLIdx] > nodes[pIdx]) {
            Idx2swap = cLIdx
        }
        if (cRIdx <= len && nodes[cRIdx] > nodes[cLIdx] || pIdx) {
            Idx2swap = cRIdx
        }
        if (Idx2swap && nodes[Idx2swap] > nodes[pIdx]) {
            this.swap(nodes, Idx2swap, pIdx);
            this.bubbleDown(Idx2swap);
        }
    };
    extractMax(pIdx = 0) {
        if (!this.values) return;
        if (this.values.length <= 1) {
            let last = this.values.pop();
            this.values = [];
            return last;
        }
        this.swap(this.values, 0, this.values.length - 1)
        const ele = this.values.pop();
        this.bubbleDown(pIdx)
        return ele
    }
}

// Test Insert()
var binHeap = new MaxBinHeap()
console.log(`1st loop `)
console.log(binHeap.values)
binHeap.insert(55)
console.log(`\n2nd loop`)
console.log(binHeap.values)
/*
    Following this execution
    [
    55, 39, 41, 18
    27, 12, 33
    ]
*/
binHeap.insert(2)
console.log(`\n3rd loop`)
console.log(binHeap.values)
/*
    Following this execution
    [
    55, 41, 39, 27,
    12, 33, 18,  2
    ]
*/

binHeap.insert(45)
console.log(`\n4th loop `)
console.log(binHeap.values)
/*
[
  55, 45, 41, 39,
  27, 12, 33, 2, 18
]
*/
