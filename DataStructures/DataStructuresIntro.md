# Data Structures Notes
## Intro to Data Structures
- Data Structures are collections of values, relationships, and functions/ops that can be applied to data, ex: array has built in functionality and methods for data manipulation and access (push, pop, reverse)
- Data Structures excel at specific actions, some more specialized while other are more generally used

- All are used: working with map or location data requires graphs, ordered list with fast inserts/removals at begin/end req linked list, web scrape nested html requires a tree, scheduler requires a binary heap
    * (es 2015 syntax) how js implements class idea [ abstraction, encapsulation, polymorphism ]
    * class = blueprint for object creation w/ defined properties, methods [ js not obj oriented, easier for class like structures ]

```javascript
class Student{
 constructor(firstName, lastName, year){
  this.firstName=firstName;
  this.lastName=lastName;
  this.grade=year;
 }
}
/*
	method to create new objs involves constructor
	class keyword
		- creates constant, no need to redefine it
*/
let firstStudent=new Student('First','Last')
// this - refers to indv instance of obj instance
```
## Instance methods
- provide functionality for a particular instance,
ex: .push() is a method that acts on indv instance of array (pattern)

```javascript
class Student{
   constructor(firstName, lastName, year){
      this.firstName=firstName;
      this.lastName=lastName;
      this.grade=year;
      this.tardies=0;
      this.scores=[]
    }
    fullName(){
        return `full name is ${this.firstName} ${this.lastName}`;
    }
    markLate(){
        this.tardies+=1;
        if(this.tardies>=3){
        return 'ur done'
        }
     return `${this.firstName} ${this.lastName} has ${this.tardies} tardies`;
    }
    addScore(score){
        this.scores.push(score)
        return this.scores
    }
    calcAvg(){
        let avg=this.scores.reduce((a,b)=>{
         return a+b/(this.scores.length);
        })
      return avg
    }
}
let firstStudent=new Student('don','man',4)
/* in console
	 firstStudent
Student {
  firstName: 'don',
  lastName: 'man',
  grade: undefined,
  tardies: 0,
  scores: []
}
 firstStudent.addScore(12,80)
[ 12 ]
 firstStudent.addScore(80)
[ 12, 80 ]
 firstStudent
Student {
  firstName: 'don',
  lastName: 'man',
  grade: undefined,
  tardies: 0,
  scores: [ 12, 80 ]
}
 firstStudent.calcAvg()
46
*/
```
## Class Methods
- uses static keyword in front of method
definition allowing the definition of methods/functionality associated to classes but not indvidual class instances
- for most ds, using instance methods
(static can’t be used through class instance, create utility functions)
```javascript
//using class methods with static keyword
class Student{
	constructor(firstName, lastName, year){
    this.firstName=firstName;
    this.lastName=lastName;
    this.grade=year;
    this.tardies=0;
    this.scores=[]
  }
	fullName(){
		return `full name is ${this.firstName} ${this.lastName}`;
	}
	//class method start with static
	static enrollStudents(...students){
		return 'Enrolling Students'
	}
}
/*
	Student.enrollStudents()
	-returns string in static method
*/
let secondStudent=new Student('second','name',3')
```

- Second Example using a Class Method
```javascript
class Point{
  constructor(x,y){
    this.x=x;
    this.y=y;
  }
  static distance(a,b){
    const dx=a.x-b.x;
    const dy=a.y-b.y;
    return ((dx**2)+(dy**2))**(1/2)
  }
}
const p1=new Point(5,5);
const p2=new Point(10,10);

/*
	Point.distance(p1,p2)
	7.0710678118654755
*/
```

## Summary of using Class Methods
```javascript
class DataStructure(){
	constructor(){
		//ds default props
	}
	someInstanceMethod(){
		//method each obj created from class are able to do
	}
}
/*
	(this refers to object created for class - instance)
	never really use static methods
	(1) classes = blueprints to make instances
	(2) class instances created w/ 'new'
	(3) constructor = special fxn ran when class is instantiated
	(4) instance methods added to classes like methods in objs
*/
```
