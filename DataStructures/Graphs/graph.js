/*
 build undirected graph
    a , b
    |
    c
Graph { adjacencylist: { A:['B','C'], B:['A'], C:['A'] }}
*/
class Graph {
    constructor() {
        this.adjacencyList = {}
    }
    remove(array, value, index = array.length - 1) {
        if (index === -1) return 'edge not found';
        if (value === array[index]) return array.splice(index, 1);
        this.remove(array, value, index - 1)
    }
    addVertex(name) {
        const list = this.adjacencyList
        if (list[name]) return 'already added'
        list[name] = [];
        return `initialized ${name}: [] `
    }
    addEdge(v1, v2) {
        const list = this.adjacencyList
        if (list[v1].includes(v2)) return 'edge already exists'
        if (!list[v1]) this.addVertex(v1)
        if (!list[v2]) this.addVertex(v2)
        list[v1].push(v2)
        list[v2].push(v1)
        return `added edge between ${v1} and ${v2}`
    }
    removeEdge(v1, v2) {
        const list = this.adjacencyList
        let [ver1, ver2] = [list[v1], list[v2]];
        if (!ver1 || !ver2) return 'vertex not found'
        this.remove(ver1, v2)
        this.remove(ver2, v1)
        return `edge removed`;
    }
    removeVertex(vertex) {
        const list = this.adjacencyList
        if (!list[vertex]) return `vertex not found`
        for (let ele of Object.keys(list)) {
            this.remove(list[ele], vertex)
        }
        delete list[vertex]
        return list;
    }
    traverse() {
        const list = this.adjacencyList
        for (let item of Object.keys(list)) {
            console.log(
                `item ${item} has connections to ${list[item]}`
            )
        }

        if (!Object.keys(list).length) {
            console.log('list is empty')
        }
        return 'done with traverse'
    }
    dfs_Recursive(start) {
        const result = [];
        const visited = {};
        const list = this.adjacencyList;
        if (!list[start]) return;
        (function dfs(vertex) {
            visited[vertex] = true;
            result.push(vertex);
            list[vertex].forEach(neighbor => {
                if (!visited[neighbor]) {
                    return dfs(neighbor)
                }
            });
        })(start);
        return result
    }
    dfs_Iterative(start, visited = {}) {
        const stack = [start]
        const result = []
        const list = this.adjacencyList
        if (!list[start]) return result;
        visited[start] = true
        let vertex
        while (stack.length) {
            vertex = stack.pop()
            result.push(vertex)
            list[vertex].forEach(neighbor => {
                if (!visited[neighbor]) {
                    visited[neighbor] = true
                    stack.push(neighbor)
                }
            });
        }
        return result
    }
    bfs(start) {
        const queue = [start]
        const visited = {}
        const result = []
        const list = this.adjacencyList
        if (!list[start]) return result;
        visited[start] = true
        let vertex
        while (queue.length) {
            vertex = queue.shift()
            result.push(vertex)
            list[vertex].forEach(neighbor => {
                if (!visited[neighbor]) {
                    visited[neighbor] = true
                    queue.push(neighbor)
                }
            });
        }
        return result;
    }
}
const graph1 = new Graph()
// console.log('adding')
// graph1.addVertex('A')
// graph1.addVertex('B')
// graph1.addVertex('C')
// graph1.addEdge('A', 'B')
// graph1.addEdge('A', 'C')
// graph1.traverse()
// // out`p`ut traverse with three items with A having edges with B, C / B having edge with A / C having edge with A
// console.log('deleting')
// graph1.removeEdge('A', 'C')
// graph1.removeEdge('A', 'B')
// graph1.removeVertex('C')
// graph1.removeVertex('B')
// graph1.removeVertex('A')
// //empty
// graph1.traverse()

// test dfs
const graph2 = new Graph()
graph2.addVertex('A')
graph2.addVertex('B')
graph2.addVertex('C')
graph2.addVertex('D')
graph2.addVertex('E')
graph2.addVertex('F')
graph2.addEdge('A', 'B')
graph2.addEdge('A', 'C')
graph2.addEdge('B', 'D')
graph2.addEdge('C', 'E')
graph2.addEdge('E', 'D')
graph2.addEdge('D', 'F')
graph2.addEdge('E', 'F')
graph2.traverse()
// [ 'A', 'B', 'D', 'E', 'C', 'F' ]
console.log(graph2.dfs_Recursive('A'))
console.log(graph2.dfs_Iterative('A'))
// [ A , B , E , (B) , C , D , (E) , F ]
console.log(graph2.bfs('A'))
