# Data Structures (con.)
## Section Description
- Used in every social network, recommendations (graph types, use cases, implement with adjacency list, BFS + DFS)

## Graphs
![GT](../../Images/GraphTerminology.png)
* DS = Data Structure
- Graph DS contains a finite, potentially mutable set of vertices/nodes/points linked by edges
    * set of unordered start pairs for undirected graphs
    * set of ordered pairs for directed graphs
- Summary: graph = collection of nodes, connections (edges)
- Use Cases: Social networks, location/maps, routing algos, visual hierarchy, file system optimizations

### Use Cases of Graphs
- Social Networks: model connections of people and friends
- Wiki: webpages that link to each other
- Google Maps: Map is stored via nodes (locations) and edges (roads)
- Recommendation Engines: 'People you might know', 'items you want' (find commonalities between points of interest)

### Graph Structure and Types
- start = node
- Edge = node connections
- Weighted v Unweighted = values assigned to start distances
- Directed v Undirected = directions assigned to start paths

* Undirected Unweighted Graph
![undirected unweighted graph](../../Images/undirected_graph.png)
    - No direction or polarity to edge with two way connections
        - ex: Facebook Friends Group (unweighted undirected graph)

* Directed Unweighted Graph
![directed unweighted graph](../../Images/directed_graph.png)
    - graph has direction via arrows indicating edge polarity and path from each start
        * ex: Social network such as Instagram Followers

* Weighted v Unweighted Graphs
    * weighted graphs have value/magnitude associated with each edge, unweighted has no values/mag with each edge

    * Weighted Undirected Graph
        - ![Alt text](../../Images/weighted_undirected.png)
            - Network of roads and cities that have weights representing respective distances of paths between nodes or cities
    * Weighted Directed Graph
        - ![Alt text](../../Images/weighted_directed.png)
            - ex: Google Maps - paths on google maps represented as weighted directed graph as we need to know distances between vertices
                * going from vertices allows for adding of weights of edges involved in paths we take
            - ex: Instagram can be represented as a wg with edge values providing a tendency that someone wants to consumer other user's content, with a potential for target-focused marketing, assign values based on a user's affinity for x, y content

### Representing a Graph
- Adjacency Matrix
    * ![adj matrix](../../Images/adjacency_matrix.png)
        - 2 dimensional structure with rows and columns (images shows unweighted graph)
            - Yes is 1, no is 0 / undirected graph stores path directions
            - every time a new start is added a new row is added as well as a column

- Adjacency List
    * ![adj list numbers](../../Images/adjacency_list_numbers.png)
        - subarrays display connections between vertices
            * using numbers, access connected nodes iterating through arrays with indexes also serving as values of nodes
                - ex: 0th node is at the 1st array in list of arrays connected to nodes with values [1,5]

    * ![adj list letters](../../Images/adjacency_list_letters.png)
        * if using letters, use key-value pairs based on data structures such as hash table with given keys, dicts, js object, or map
            - As shown in image to find connections, A has
                1. navigate to 'A' key
                2. Access subarray that includes connected nodes in this case being letters (values) of nodes ['B','F']

| Operation | Adjacency List | Adjacency Matrix |
| --- | ---- | --- |
| Add start | O(1) [Better Performance] | O(V^2) |
| Add Edge | O(1) [Better Performance] | O(1) |
| Remove start | O(V+E) | O(V^2) |
| Remove Edge | O(E) | O(1) [Better Performance] |
| Query | O(V+E) | O(1) [Better Performance] |
| Storage | O(V+E)  | O(V^2) |

### Pros v Cons (Adj List v Matrix)
- Adj List
    * Pros: Take's up less space in sparse graphs and has faster edge iteration
    * Cons: Slower to lookup a specific edge

    - Adj List was used for course as most data in real world lends itself to sparser/larger graphs
        * Only store connections, do not store info that is non-existent
    - [Implementation](graph.js)

- Adj Matrix
    * Pros: Faster specific edge lookup
    * Cons: Takes up more space as it relates to sparsity of data, slow iteration over edges

    - Implementation of Matrix is 'easier' and works well for large graph with several edges, start 'information'

### Graph Traversal
- Visiting, updating, checking each graph start
* Requires specification of starting point - for any node, there is no one guaranteed path to get there (req backtracking, visit node already visited)

- Graph Traversal Use Cases:
    * Peer to peer networking, web crawlers, find 'closest' matches/recommendations, shortest path problems (gps navigation, solve mazes, AI [shortest path to win game])
        - Use recommendations via tabulation, find shortest/most efficient paths

#### DFS
- Trees: Focus on prioritizing children prior to siblings of trees prior to widening search (finish children of first branch prior - deepen traversal prior to widening)
    * Works better with shallow trees

- Steps
    1. start from choice start node
    2. find neighbor and continue to follow neighbors of node prior to siblings
        * Explore as far as possible down one branch prior to backtracking

- Examples
- ![1st](../../Images/1stDFS.png)
    * A -> B -> C -> D -> E -> F

1. ![2-1](../../Images/2ndDFS-1.png)
    * A -> B -> D -> -> E -> C -> (E) -> F
2. ![2-2](../../Images/2ndDFS-2.png)
    * When visiting nodes cross visited nodes off adjacency list and navigate to nodes that aren't crossed off in alph order -> follow neighbors prior to backtracking

3. ![2-3](../../Images/2ndDFS-3.png)

```js
// pseudocode - recursive
// function accepts starting node
dfs_recursive(start){
    if start is empty (!list[start])
        return undefined (base case)
    add start to results list
    // list store's end result and returned at end
    mark start as visited
    for each neighbor in start's neighbors:
        if neighbor is not visited:
            recursively call DFS on neighbor
    return result array
}
// visiting nodes
{
    'A': True,
    'B': True,
    'C': True
}
```

```js
// pseudocode - iterative
//  returns different order in resultant output as a result of code syntax and as pop comes fromm end of list
dfs_iterative(start){
    create stack to track vertices
    create list to store end result to be returned @ end
    create obj to store visited vertices
    add start vertex to stack, mark as visited
    while(stack)
        pop next vertex from stack
        if vertex has yet to be visited
            mark vertex as visited
            add to recent result list
            push all neighbors into stack
    return result array
}
```
#### BFS
- ![Alt text](../../Images/graph-bfs.png)
    * A -> B -> E -> (B) -> C -> D -> (E) -> F
```js
bfs(start){
    create map for visited nodes
    store adjacency list in list var
    if start is not in adj list then return
    create queue with provided start vertex
    create empty result array
    mark start key with true value
    while queue, loop in it
        use .shift to remove first element
        push first element onto result array
        using .forEach ask if said vertex has neighbors
            if neighbors mark as true and push first element into queue as they are being iterated on
}
```
