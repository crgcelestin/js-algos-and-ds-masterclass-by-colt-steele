//with more efficient priority queue implementation
class WeightedGraph {
    constructor() {
        this.adjacencyList = {};
    }
    addVertex(name) {
        const list = this.adjacencyList;
        if (list[name]) return 'already addded'
        list[name] = [];
        return `added ${name}:[]`
    }
    addEdge(v1, v2, weight) {
        const list = this.adjacencyList;
        if (list[v1].includes(v2)) return 'edge already exists'
        if (!list[v1]) this.addVertex(v1);
        if (!list[v2]) this.addVertex(v2);
        list[v1].push({ node: v2, weight });
        list[v2].push({ node: v1, weight });
        return `added edge between ${v1} + ${v2}`
    }
    dijkstra(start, end) {
        const list = this.adjacencyList;
        const distances = {};
        const previous = {};
        const nodes = new PQ();
        let path = [] //return at end
        let smallest;
        for (let vertex in list) {
            previous[vertex] = null;
            vertex === start ?
                (distances[vertex] = 0,
                    nodes.enqueue(vertex, 0))
                :
                (distances[vertex] = Infinity,
                    nodes.enqueue(vertex, Infinity))
        }
        while (nodes.values.length) {
            smallest = nodes.dequeue().val;
            if (smallest === end) {
                while (previous[smallest]) {
                    path.push(smallest);
                    smallest = previous[smallest];
                }
                break;
            }
            if (smallest || distances[smallest] !== Infinity) {
                for (let neighbor in list[smallest]) {
                    let next = list[smallest][neighbor]
                    let candidate = distances[smallest] + next.weight
                    let nextNeighbor = next.node
                    if (candidate < distances[nextNeighbor]) {
                        distances[nextNeighbor] = candidate;
                        previous[nextNeighbor] = smallest;
                        nodes.enqueue(nextNeighbor, candidate);
                    }
                }
            }
        }
        return path.concat(smallest).reverse();
    }
}

class PQ {
    constructor() {
        this.values = [];
    }
    enqueue(val, priority) {
        let newNode = new Node(val, priority);
        this.values.push(newNode);
        this.bubbleUp();
    }
    bubbleUp() {
        let idx = this.values.length - 1;
        const element = this.values[idx];
        while (idx > 0) {
            let parentIdx = Math.floor((idx - 1) / 2);
            let parent = this.values[parentIdx];
            if (element.priority >= parent.priority) break;
            this.values[parentIdx] = element;
            this.values[idx] = parent;
            idx = parentIdx;
        }
    }
    dequeue() {
        const min = this.values[0];
        const end = this.values.pop();
        if (this.values.length > 0) {
            this.values[0] = end;
            this.sinkDown();
        }
        return min;
    }
    sinkDown() {
        let idx = 0;
        const length = this.values.length;
        const element = this.values[0];
        while (true) {
            let leftChildIdx = 2 * idx + 1;
            let rightChildIdx = 2 * idx + 2;
            let leftChild, rightChild;
            let swap = null;

            if (leftChildIdx < length) {
                leftChild = this.values[leftChildIdx];
                if (leftChild.priority < element.priority) {
                    swap = leftChildIdx;
                }
            }
            if (rightChildIdx < length) {
                rightChild = this.values[rightChildIdx];
                if (
                    (swap === null && rightChild.priority < element.priority) ||
                    (swap !== null && rightChild.priority < leftChild.priority)
                ) {
                    swap = rightChildIdx;
                }
            }
            if (swap === null) break;
            this.values[idx] = this.values[swap];
            this.values[swap] = element;
            idx = swap;
        }
    }
}

class Node {
    constructor(val, priority) {
        this.val = val;
        this.priority = priority;
    }
}


const graph = new WeightedGraph()
graph.addVertex("A");
graph.addVertex("B");
graph.addVertex("C");
graph.addVertex("D");
graph.addVertex("E");
graph.addVertex("F");

graph.addEdge("A", "B", 4);
graph.addEdge("A", "C", 2);
graph.addEdge("B", "E", 3);
graph.addEdge("C", "D", 2);
graph.addEdge("C", "F", 4);
graph.addEdge("D", "E", 3);
graph.addEdge("D", "F", 1);
graph.addEdge("E", "F", 1);
