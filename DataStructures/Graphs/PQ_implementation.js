class PQ {
    constructor() {
        this.values = []
    }
    enqueue(val, priority) {
        this.values.push({
            val, priority
        })
    }
    dequeue() {
        return this.values.shift();
    }
    sort() {
        this.values.sort((a, b) => a.priority - b.priority)
    };
}

var pq = new PQ();
pq.enqueue('B', 3);
pq.enqueue('C', 5);
pq.enqueue('D', 2);
pq.enqueue('Q', 20);

/*
pq.values

[
  { val: 'D', priority: 2 },
  { val: 'B', priority: 3 },
  { val: 'C', priority: 5 },
  { val: 'Q', priority: 20 }
]
*/
