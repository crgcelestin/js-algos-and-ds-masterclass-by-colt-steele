# [YW BLOG - GRAPHS](https://blog.yoshuawuyts.com/graphs/)
- Graph = umbrella containing various data structures, there are several types with various properties
- Inherently graphs are about expressing relationships

## Primer
### Relationships
- Any type of relationship can be express in 3 types:
    * 1 to 1 (1:1), 1 to many (1:m, m:1), many to many (m:n)
### Graphs
- Graph structure is one of 'nodes' and relationships (vertices, edges)
    * Great structure for representing the connections between entities like users and the idea of following/subscribing to others
    * graph dbs use terms: triples, quads
        - triples: 1 slot to store data / relationship and quads: 2 slots to store data / relationship (distinction between A -> B, A <- B)
### Cycles
- graphs often use the terms 'acyclic', 'cyclic', 'cycles'
    * refers to if a graph relationship can be followed to create loops
    * A -> B -> A is cyclic
- Even if graph can be 'cyclic', there is a possibility of this quality in future
    * if a structure guarantees no loops -> acyclic

## Directed Graph
![Alt text](cyclic_graph.png)
| property | value |
| - | - |
| cycles | possible |
| max child per node | unconstrained |
| max parents per node | unconstrained |

### About
- Most basic type is cyclic and has fewest constraints
    * most flexible, fewest guarantees
- Every node can have m:n relationship (many to many), nodes can link to selves directly
- Any connection is allowed and can make it hard ot keep track as any new connections potentially means change of graph assumptions
    * in order to make significant conclusions, graph req re-eval

- Example: Social Network (any person can have relationship with others w/ no constrains on identity or number of relationships)

## Acyclic Graphs
![Alt text](acyclic_graph.png)
| property | value |
| - | - |
| cycles | not possible |
| max child per node | unconstrained |
| max parents per node | unconstrained |

### About
- Acyclic graph similar to regular graph, but can't have cycles
    * useful when using computers to guarantee no infinite loops can occur

- Defined by establishing hierarchy of parents, children -> A is the parent of [B,C,D] and vice versa
    * Cycles are prevented by these rules:
        - any node can have several parent nodes and several child nodes
        - parent or sibling node can't also be a child node

- Example: Software dependencies where any dependency can have other dependencies but if there is dependency cycle it can't resolve (circular import)

## Trees
![Alt text](tree.png)
| property | value |
| - | - |
| cycles | no |
| max child / node | unconstrained |
| max parents / node | 1 |

### About
- simplification of acyclic, impose additional constraint that each node has 1 parent (root has none)
    * 'binary trees' - each node has up to 2 children

- Examples
    * Rust data model - any data piece must have only 1 parent and can share several references to several child functions
        * child fxns can't outlive parent lifetime
    * Function execution forms tree - any fxn call several fxns, no single fxn originates from 2 callers at same time creating a tree
        * Expressing concurrency as tre = easier reasoning

## Logs
![Alt text](logs.png)
| property | value |
| - | - |
| cycles | no |
| max child / node | 1 |
| max parents / node | 1 |

### About
- simplest type of graph
    * every node has 1 parent and at most 1 child (except for root)
    * provide absolute ordering of items

- Example
    * common use are arrays in programming, byte sequence in memory
    * for dbs, lists are great to track events that can be processed, replayed into several types to be queried
    * __Generally__ logs are used for anything involving time
        - accounting data: tracked as events series where money is in + out
        * 'feed' involves tracking data over time
