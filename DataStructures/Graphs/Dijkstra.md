# Graphs (con.)
## Disclaimer
- Typically performed with weighted directed graph, actual implementation is a weighted undirected graph [{code}](weightedGraph.js)

## Description
- Objectives:
    * understand significance, implement weighted graph, steps, implement with naive Priority Queue, binary heap PQ
- Requires graph, priority queue (binary heap)

* Dijkstra's is a famous, widely used algorithm and allows use to find shortest path between graph vertices
![Alt text](./Notes/ds.png)
    - ^ real world implementation using a weighted graph
    - Typically used with GPS(find fastest route), network routing (shortest open path for data transmission), biology (model spread of viruses among humans), airline tickets (cheapest route tod destination), etc.
    * Ex: Find shortest path from A to E
        - Graph w/ 6 vertices and 8 edges and weights communicate via respective numbers (4 step algo)
        - Approach
            1. Every time a node is visited, pick node with smallest know distance to visit first
            2. once we move to next node to visit, look at each neighbor
            3. for each neighbor node, calculate distance bby summing total edge weights leading to node being checked from starting node
            4. if new total distance to node is less than previous total, store new shorter distance for node
    * ![Alt text](image.png)
    * ![Alt text](image-1.png)
    * ![Alt text](image-2.png)
    1. Shortest distance from A to A is 0 with 2 neighbors being B and C
    2. B has a distance of 4 from A, update B and set it to A then A-> C is 2, update C to A
    3. C to D is 4 and set D to C as thats how we got there from A
    4. C to F is 6 (2+4) and update F to C
    - To get to E from A, A→ B → E which is 7 so updated E to be B
    - Left over nodes [D,E,F], Done [A, C, B]
    - D is smallest at 4, then E
    - D→ E has total of 7 (ignored), then D to F and total is 5 so update 6 to 5 and update F to D
        * Update visited to be [A,C,B,D]
        * Left unvisited is E, F
        * Shortest path between E and F is 5 (F), F→ E is 6, while E has 7 so update it to 6
        * Update E to F
   6. Done finding shortest path

### Priority Q Implementation - Simple
- PQ is essential for selecting next smallest value
    * if there are 1*10^6 nodes, it is difficult and requires DS implementation
    * PQ allows re-sorts every time new value, priority are enqueued
        - get smallest value regardless of initial order, optimize code using min binary heap
        - [PQ](PQ_implementation.js)

## D's Pseudo-Code
1. Fxn accepts starting, ending vertex
2. Create obj (distances) and set each key to be every vertex in the adjacency list w/ value of infinity, except for starting vertex which has 0 value
3. After setting value in distances object, add each vertex with priority of infinity to PQ, except starting vertex, which has a value of 0 as thats the start
4. Create other obj called previous, set each key to be every vertex in adjacency list with value of null
5. Start looping as long there is anything in PQ
    1. Dequeue vertex from priority queue
    2. If vertex is same as ending vertex → done (return)
    3. Else loop through each value in adjacency list of vertex
        1. Calc distance to vertex from starting vertex
        2. If distance is < currently stored in distances object
            1. Update distances object with new lower distance
            2. Update previous object to contain vertex
            3. Enqueue vertex with total distance from start node

- Version of sole method
    * [Version 1](version1.js)

- ! ACTUAL DIJKSTRA'S IMPLEMENTATION !
    * [Implementation](Dijkstra.js)

* RECAP
    - Graphs = collections of vertices connected by edges, can be represented by using adjacency lists, matrices, etc.
        * adj list used for implementation due to simplicity, performance tradeoff

    - Graphs can contain weights, directions, cycles
        * traversed with BFS, DFS

    - Shortest path algos like d's can be altered with heuristic to achieve better results like with A Star
