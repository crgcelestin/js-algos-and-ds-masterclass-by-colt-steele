# Data Structures (con.)
## Section Description
- What a hash table is, hash algo, good hash algo, collisions in hash tables, handle collisions with separate chaining/linear probing

## HashTables
![hash](../../Images/HashTable.png)
ds = data structure, ht = hash table

- HTs allow for the storage of key-value pairs, keys are not ordered and is fast for finding values, adding and removing values
    * An example being that we have colors associated to hex numbers
    * a hashtable allows us to hash that key, store key via hash and value (hex number) at a given index outputted by hash function
    * we can perform a level of encryption with this implementation

- Nearly every language has a hash table ds and due to speed they are commonly used
    * py (dictionaries), js (objects, maps), [java, go, scala] (maps), ruby (hashes)
- Objects in js are only able to use strings as keys, in py you can use tuples as well as keys

> - Implementing own version of key-value store ds:
>   - use readable keys in order to access values vs indices e.g. colors['cyan] -> reinvents basic mapping of key value pairs

> - Hash Table implementation involves an array -> convert keys to valid array indices (hash function)
>   * [string -> hashed string -> index integer]
> - Require hash function to point to same indices

- Basic hash functions take in data of arbitrary size, map to data of fixed size, using hash() and functions are typically 1 way
    * input -> data of fixed size

- Good hash function [insecure in regards to cryptography]
    1. fast (constant time: O(1))
    2. doesn't cluster outputs at specific indices i.e distributes uniformly
    3. deterministic [same input -> same output]

## Basic Hash Function
* use utf16 codes for characters [.charCodeAt()]
* 'a'.charCodeAt(0) -> 97 [charCodeAt gives utf conversion of char at index provided in parentheses]
```js
function hash(key, len){
    let total=0;
    for(let char of key){
        let val = char.charCodeAt(0)-96
        // req valid index between 0 and len(array length)
        total=(total+val)%len
    }
    return total;
}
hash('orangered',10); // 7
```
* Problems with this example: only hashes strings, not constant time as inputs increase linearly with key length, needs to be more random as data will cluster fairly easier

- Refine Hash
    * Do not loop over every character with the possibility that there can be several million characters -> choose a smaller subset and loop over it -> add in a minimum `Math.min(key.length, 100)` -> as we want hash to be quick
    * add in prime numbers - hash functions take advantage of primes as it spreads keys out uniformly and puts values into array with prime length
    ```js
    function hashv2(key, len){
        let total=0;
        let WEIRD_PRIME=31
        for(let i=0; i<Math.min(key.length, 100);i++){
            let char=key[i]
            let value=char.charCodeAt(0)-96
            total=(total+value*WEIRD_PRIME)%len
        }
        return total
    }
    ```
    - we use % and weird number to offset indices and change allows for a better distribution
    - hash now involves a prime number multiple and array length
        * restraining the loop to 100 allows for an optimized hash function

## Collision Handling
ll = linked list
- Collisions are inevitable even with larger arrays and improved hash functions
    * in order to deal with collisions we can use (1) separate chaining or (2) linear probing

    1. ![SC](../../Images/SeparateChaining.png)
        - Separate Chaining (__DEFAULT__)
            - at each array index, store values with a ds like array or ll which allow for the storage of several key-value pairs at same index
            - image shows the ds storing 2 nested arrays
            * hash `salmon` -> index 4 -> loop through index 4 -> find salmon with corresponding value

    2. ![LP](../../Images/linearProbing.png)
        - Linear Probing
            - when finding a collision, search through array to find next empty lot which means we don't have to store hashes in a nested structure
            - if multiple values when hashed outputs the same index, look for next space availability

## BIG O
- Hash Table
    * insertion, deletion, and access are all O(1) at avg case and worst case they are O(n) for all methods
    - (recap)
        * hash tables is a collection of key-value pairs, can find values quickly with key, add new pairs rapidly
    - HTs store data in larger array with hashing and a good ht is fast, uniform in terms of key distribution, and deterministic
    - separate chaining (preferred) and linear probing are strategies to allow for dealing with 2 keys that may hash to the same index
