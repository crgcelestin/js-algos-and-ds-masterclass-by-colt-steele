// HashTable Class
class HashTable {
    constructor(size = 53) {
        this.keyMap = new Array(size)
    }
    _hash(key) {
        let total = 0;
        let WEIRD_PRIME = 31;
        for (let i = 0; i < Math.min(key.length, 100); i++) {
            let char = key[i]
            // optional: let charCode=Math.abs(char.charCode(0)-96)
            let charCode = char.charCodeAt(0) - 96
            let value = charCode < 0 ? (this.keyMap.length - charCode) : charCode
            total = (total + value * WEIRD_PRIME) % this.keyMap.length;
        }
        return total
    }
    /*

        Set -
            (1) accepts key, value
            (2) hash key
            (3) store key-value pair in hash table via separate chaining

    */
    set(key, value) {
        const index = this._hash(key)
        if (!this.keyMap[index]) {
            this.keyMap[index] = [[key, value]]
        } else {
            /*
                check if key exists in array, update value
                instead of adding new pair

                i.e if the same key is trying to be inserted provided with a new value then we should just update the value of a given key

            */
            let found = false
            //iterating through nested ds at index
            const current = this.keyMap[index]
            for (let i = 0; i < current.length; i++) {
                // key stored at 0th index, value at 1st index in the array of key value subarrays
                if (current[i][0] === key) {
                    current[i][1] = value;
                    found = true;
                    break;
                }
                if (!found) {
                    current.push([key, value])
                }
            }
        }
        return this.keyMap
    }
    traverse() {
        for (let c = 0; c < this.keyMap.length; c++) {
            const current = this.keyMap[c]
            if (current) {
                for (let i = 0; i < current.length; i++) {
                    console.log(`key: ${current[i][0]}, ${current[i][1]}`)
                }
            }
        }
        return 'traversed'
    }
    get_pair(key) {
        const index = this._hash(key)
        const searched = {}
        const current = this.keyMap[index]
        if (current) {
            for (let i = 0; i < current.length; i++) {
                if (current[i][0] === key) {
                    searched[current[i][0]] = current[i][1]
                }
            }
        }
        // return dict containing key as key, value as value from index matching said key
        return searched;
    }
    get_value(key) {
        const index = this._hash(key)
        const current = this.keyMap[index]
        if (current) {
            for (let i = 0; i < current.length; i++) {
                if (current[i][0] === key) {
                    return current[i][1]
                }
            }
        }
        return -1
    }
    delete_pair(key) {
        const index = this._hash(key)
        const current = this.keyMap[index]
        if (current) {
            for (let i = 0; i < current.length; i++) {
                if (current[i][0] === key) {
                    this.keyMap.splice(i, 1)
                    return `deleted key ${key}, deleted value ${current[i][1]}`
                }
            }
        }
        // return dict containing key as key, value as value from index matching said key
        return this.keyMap.traverse()
    }
    keys() {
        const keys = this.keyMap.reduce((result, array) => {
            array.forEach(([key, value]) => {
                if (!result.includes(key)) {
                    result.push(key)
                }
            })
            return result
        }, [])
        return keys
    }
    values() {
        const values = this.keyMap.reduce((result, array) => {
            array.forEach(([key, value]) => {
                if (!result.includes(value)) {
                    result.push(value)
                }
            })
            return result
        }, [])
        return values
    }
}

Table = new HashTable()
console.log(
    Table._hash(
        'Left'
    )
)
console.log(
    Table.set(
        'Left', 'f33'
    )
)

console.log(
    Table.traverse()
)

console.log(
    Table.delete_pair(
        'Left'
    )
)
/*

-- iterate over entire hashtable --

Table.keys().forEach(function (key) {
    console.log(ht.get(key));
})

*/
