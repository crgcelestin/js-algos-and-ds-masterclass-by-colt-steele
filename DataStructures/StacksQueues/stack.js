class Node {
    constructor(value) {
        this.value = value;
        this.next = null;
    }
}
//first in -> last out, last in -> first out (FILO)
class Stack {
    constructor() {
        this.first = null;
        this.last = null;
        this.size = 0;
    }
    //add value to stack's top
    push(value) {
        var newNode = new Node(value)
        if (!this.size) {
            this.first = newNode
            this.last = newNode
        } else {
            newNode.next = this.first
            this.first = newNode
        }
        this.size += 1
        return this
    }
    //remove value from top of stack
    pop() {
        if (!this.size) return;
        var temp = this.first;
        if (this.size === 1) {
            this.last = null
            this.first = this.last
        }
        this.first = temp.next
        this.size -= 1
        return temp.value
    }
}

var first = new Stack()
