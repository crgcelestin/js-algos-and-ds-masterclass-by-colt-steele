# Data Structures (Con.)
## Stack
![Stack](../../Images/Stack.png)
- LIFO (Last In First Out <->First in Last Out) Data Structure, last element added to stack is first element to be removed (stack of plates: as you pile items/plates, topmost gets removed first) [ A clear example: call stack is a straightforward implementation ]

- Stacks are used in managing function calls, undo/redo, routing (history object) is treated as a stack, trees and graphs are also types of stacks

### Multiple Methods of Stack Implementation
```javascript
var stack=[]

//addition of 2 elements to start of stack
stack.push('google')
stack.push('insta')

/*
	stack ['google', 'insta']
*/

//removal from end of stack
stack.pop()

/*
	Using methods like unshift, shift
	hurt performance of algos especially if
	length of list grows
	Both methods require re-indexing of arrays
	using pop and push, involves just adding new indices
*/

//add element to front of stack
stack.unshift('gtg')

//remove element from start
stack.shift()

/*
	!
    It is a bad idea to use an array for a stack
	due to bad efficiency, use LL as we have no
	need for indices
    !
*/
```

### Stack LL Implementation Recap

[Linked List Stack](stack.js)

Big O of Stack Structure
* Stacks - LIFO data structures where last value in
                    is first value out
- Insertion [ push(val) ] - O(1)
- Removal [ pop() ] - O(1)
- Stacks are used to handle function invocations (call stack)
or for operations like undo/redo, routing (remember pages
that are visited and go back/forward), .etc
- Not built in JS DS, but are simple in implementation

## Queue
- FIFO (First In First Out <-> Last In Last Out)
- Equivalent to a real life line where tasks are prioritized by their arrival
- In programming: Queues structure for multiplayer, background tasks, uploading resources, best analogy: printing/task process

```javascript
// Building Queue with an array
// adding via enqueue, removing via dequeue

var q = []
//add from end of stack, remove from beginning
//enqueue
q.push(val)
//dequeue
q.shfit()

//or
//add at begin of stack, remove from end
// enqueue
q.unshift(val)
// dequeue
q.pop()
```

### Queue LL Implementation Analysis
[Linked List Queue](queue.js)
- 	Insertion and Removal via enqueue, dequeue are both
	Big O(1) constant time

- Queue is a FIFO DS, with all elements being first in first out as it pertains to addition and removal
- Queues are useful for processing tasks and are foundational for complex Data Structures
- Insertion and removal can be done in O(1) time
