//LL Q implementation
class Node {
    constructor(value) {
        this.value = value;
        this.next = null;
    }
}

// first in -> first out, last in -> last out (FIFO)
class Queue {
    constructor() {
        this.first = null;
        this.last = null;
        this.size = 0;
    }
    // add to end of queue
    //push
    enqueue(val) {
        var newNode = new Node(val);
        if (!this.size) {
            this.first = newNode
            this.last = newNode
        } else {
            this.last.next = newNode
            this.last = newNode
        }
        this.size += 1
        return this
    }
    //removing from start of q
    //shift
    dequeue() {
        if (!this.size) return;
        var temp = this.first;
        if (this.size == 1) {
            this.last = null;
        }
        this.first = this.first.next;
        this.size--;
        return temp.value;
    }
}

var queue = new Queue()
