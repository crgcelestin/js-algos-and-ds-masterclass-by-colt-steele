class Node():
    def __init__(self, value, next=None, prev=None):
        self.value=value
        self.next=next
        self.prev=prev

class Double_LinkedList():
    def __init__(self, length=0):
        self.head=None
        self.tail=None
        self.length=length
    def traverse(self):
        pass
    def insert(self, value, position=None):
        pass
    def search(self, value):
        pass
    def pop(self):
        pass
    def shift(self):
        pass
    def unshift(self, value):
        pass
    def get(self, index):
        pass
    def set(self, value, index):
        pass
    def remove(self, index):
        pass
    def reverse(self):
        pass
