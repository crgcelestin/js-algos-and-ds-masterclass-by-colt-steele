class Node {
    constructor(value) {
        this.value = value;
        this.next = null;
        this.prev = null;
    }
}

class DoublyLinkedList {
    constructor() {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }
    traverse() {
        let start = this.head
        while (start) {
            console.log(start.value)
            start = start.next;
        }
        return 'end of LL'
    }
    push(value) {
        var newNode = new Node(value)
        if (!this.head) {
            this.head = newNode
            this.tail = newNode
        } else {
            this.tail.next = newNode
            newNode.prev = this.tail
            this.tail = newNode
        }
        this.length += 1
        return this
    }
    search(val) {
        let curr = this.head
        for (let i = 0; i < this.length; i++) {
            if (curr.value === val) return i;
            else curr = curr.next;
        }
        return 'not found'
    }
    pop() {
        if (!this.head) return;
        var end = this.tail
        if (this.length === 1) {
            this.head = null
            this.tail = null
        } else {
            this.tail = this.tail.prev
            this.tail.next = null
            //sever popped node's connections
            end.prev = null
        }
        this.length -= 1
        return end.value
    }
    shift() {
        if (!this.length) return;
        var old = this.head
        if (this.length === 1) {
            this.head = null
            this.tail = null
        } else {
            this.head = old.next
            this.head.prev = null
            old.next = null
            //sever old head's connections
        }
        this.length -= 1
        return old
    }
    unshift(value) {
        var newNode = new Node(value)
        if (!this.length) {
            this.push(value)
        } else {
            this.head.prev = newNode
            newNode.next = this.head
            this.head = newNode
        }
        this.length += 1
        return this
    }
    get(index) {
        if (index >= this.length || index < -this.length) throw new Error('index out of range');
        if (index < 0) index += this.length;
        var middle = this.length / 2;
        var current, count;
        if (index <= middle) {
            current = this.head
            count = 0
            while (count != index) {
                current = current.next
                count += 1
            }
        } else {
            count = this.length - 1;
            current = this.tail;
            while (count !== index) {
                current = current.prev;
                count -= 1;
            }
        }
        return current;
    }
    set(val, index) {
        var node = this.get(index)
        if (!node) return false;
        node.value = val
        return true;
    }
    insert(val, index) {
        var newNode = new Node(val)
        if (index > this.length || index <= -this.length) throw new Error('index out of range');
        if (index === 0) {
            return !!this.unshift(val);
        } else if (index === this.length) {
            return !!this.push(val);
        } else {
            var prevNode = index > 0 ? this.get(index - 1) : this.get(index)
            var temp = prevNode.next
            prevNode.next = newNode
            newNode.prev = prevNode
            newNode.next = temp
            temp.prev = newNode
        }
        this.length += 1
        return this
    }
    remove(index) {
        if (index >= this.length || index <= -this.length) throw new Error('index out of range');
        var curr = this.get(index)
        if (index === 0) {
            !!this.shift();
        } else if (index === this.length - 1) {
            !!this.pop();
        } else {
            var prevNode = index > 0 ? this.get(index - 1) : this.get(index)
            var currVal = this.get(index).value
            if (prevNode.next === null) {
                !!this.pop()
            } else {
                prevNode.prev.next = prevNode.next
                prevNode.next.prev = prevNode.prev
            }
        }

        curr.next = null
        curr.prev = null
        this.length -= 1
        return curr
    }
    reverse() {
        var current = this.head
        this.head = this.tail
        var prev = null
        var next = null
        for (let i = 0; i < this.length; i++) {
            next = current.next
            current.next = prev
            current.prev = next
            prev = current
            current = next
        }
        return this
    }
}

var first = new DoublyLinkedList()
var fN = new Node(1)
first.push(fN)
var second = new Node(2)
first.push(second)
var zero = new Node(0)
