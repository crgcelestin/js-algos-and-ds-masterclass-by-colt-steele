## Singly Linked Lists
![LL](Images/singleLL.png)
- __Lists__ don’t have indices
and are connected via nodes with next/link pointer,
Random access not allowed, but insertion and deletion are quick vs
__Arrays__ which are indexed in order and with Arrays insertion and deletion are expensive
and there is quick access at specific index

- Use LLs if one cares about performance regarding insertion and deletion
of data in array-like format

(Big O of Singly LL)

- Insertion is O(1)
    - Great performance, easy to perform [ push it on to end (push), beginning (unshift) ]
    - Push is O(N)
- Removal is O(1) or O(N)
    - Beginning is O(1) being shift and at end its pop O(N), deleteMiddleNode [ O(N) ]
- Searching is O(N)
    - worst case is end and thus is O(N) operations, search() i.e finding node that has a node.value=value
- Accessing is O(N)
    - similar to searching complexity, as list grows so does ops for accessing, getting desired node at x position, get(), set(), insert()
- ![insert](Images/insert.png)
- ![remove](Images/remove.png)
- ![reverse](Images/reverse.png)


(LL Recap)

- __SLLs__ are great alts where insertion, deletion at beginning of dataset are frequent
- __Arrays__ have built in index vs LLs which don’t
- Idea of list data structure containing nodes = foundation of stacks, queues


## Doubly Linked Lists
![DoubleLL](Images/doublyLL.png)

### Objectives
- (1) Construct doubly LL, (2) Compare and contrast doubly and single LLS, (3) Implement basic ops on Double LL

### Notes
- Double LL - Every node has pointer that links to previous node and next

- Makes pop and removal processes far easier and straightforward, does impact the complication of method implementation accounting for 2 pointers

- Drawback: Double LL takes more memory due to increased flexibility (no indices, order is decided by .next pointers that connect nodes) points to next and item behind it

### Big O of Doubly Linked Lists
- Insertion is O(1) same to LL, Removal is O(1) [ for pushing/popping, unshifting/shifting ] for DLL due to no need for iteration, Search is O(N) with finding a node with said value, accessing with get is also O(N)
- Browser history is done with doubly linked lists, as there is additional pointer to previous nodes
- Better vs singly linked lists as finding nodes can be done in half the time, but they take more memory due to additional pointer
