// SS - not good when compared to other algos not even its cousin Bubble
// Similar to BS and places small values into sorted position vs biggest

/*
    sc: O(N^2) due to 2 loops for comparisons, tc: O(1) as swapping occurs in place
*/

const swap = (arr, idx1, idx2) => {
    return [arr[idx1], arr[idx2]] = [arr[idx2], arr[idx1]]
}

const selectionSort = (arr) => {
    var len = arr.length
    var noSwaps;
    for (let i = 0; i < len - 1; i++) {
        var min = i;
        noSwaps = true
        for (let j = i + 1; j < len; j++) {
            if (arr[min] > arr[j]) {
                min = j;
                noSwaps = false;
            }
        }
        if (i !== min) swap(arr, i, min);
        if (noSwaps) break;
    }
    return arr;
}

/*

    line 15- iterate through list starting with 1st element
    line 16,17 - store 1st element as smallest, establish a noswaps var that breaks out of loop when no swaps take place for optimization
    line 18 - inner loop starts at 2nd element in order to allow for comparisons
    line 19 - create comparison between min and element following for swaps, set min = j if smaller then set noSwaps flag to false
    line 24 - after for loop, swap the i pointer with min as it is no longer the smallest element in the array
*/
