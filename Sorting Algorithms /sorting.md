# Sorting Notes
__Includes__
- Fundamental Sorting
    - Bubble, Selection, Insertion Sort
- Advanced Sorting
    - Merge Sort, Quick Sort, Radix

## Why It's Crucial
- Sorting is a process of rearranging a collection of items typically in an array so items are in a desired order (ascending, descending, movie objects based on release year, revenue)
- There are several sorting methods with own advantages and disadvantages that have some quirks. It's critical to understand how to navigate such properties
    - Simpler sorting algorithms are advantaged when data is nearly sorted as compared to other advanced algorithms like heap, merge, etc
```javascript
    ['steele','colt','algos'].sort();
    //sorts names by alph
    [5,4,10].sort():
    //does not result in correct numeric ordering sorting

    //sort nums in ascending order
    const NumCompare=(num1, num2)=>{
        return num1-num2
        //sorts array in ascending order
        //reverse is num2-num1 (descending order)
    }
    [6,4,15,10].sort(numCompare)
    //[4,6,10,15]

    //return string in ascending order by their length
    function LenCompare(str1, str2){
        return str1.length-str2.length
    }
    ['arr','with','strings'].sort(LenCompare)
```


## Fundamental Algo Implementation Notes
- Perform best with small data sets

### __Bubble Sort__
> [ Bubble Sort Implementation](BubbleSort.js)

- Largest values bubble to top/end of array
    - Time Complexity: O(N^2) worst case as there are 2 for loops that involve n elements
    - Space Complexity: Sorting is performed in place and generally one extra variable is used to store temporary location for swapping successive items resulting in a SC of O(1) constant time

### __Selection Sort__
> [Selection Sort Implementation](SelectionSort.js)
- Similar to Bubble Sort but places smaller values into sorted positions vs biggest
    - Time Complexity: O(N^2)
    - Space Complexity: O(1) as only 2 variables are required to enable swapping of elements with one variable keeping track of smallest element in array


### __Insertion Sort__
> [Insertion Sort Implementation](InsertionSort.js)
- Builds sort by creating larger half that is always sorted
    - Time Complexity: O(N^2)
    - Space Complexity: Same as Bubble Sort being O(1)

### __Conclusion__
-  Sorting is fundamental and each of the basic sorts are roughly the same in terms of equivalence in regards to time and space complexity
- Quadratic average time complexities that excel w/ small data sets

## Advanced Implementation Notes
- Several limitations of previous fundamental sorting algos
    - Previous algos don't scale: bubble sort with 100x average element length take time to sort
    - Need better methods to sort large arrays quickly
    - More adv algos are less simple and take longer for comprehension/implementation

        ```javascript
        var data=Array.apply(null, {length:10000}.map(Function.call, Math.random)
        bubbleSort(data);
        /*
        quadratic time complexity
        means Bubble Sort takes much more time
        compared to Merge Sort
        */
        mergeSort(data); //O(n log n)?
        ```

### __Merge Sort__
![Merge](../Images/MergeSort.png)
[Merge Sort](MergeSort.js)
-  Combo of merging and sorting that exploits fact that arrays of 0 or 1 element are sorted
- One can decompose an array into smaller arrays of 0 or 1 ele(s) then build to new sorted array with Divide and Conquer approach
- Extremely good for random and reversed data

    - Time Complexity: O(n log n) as there are O(n) comparisons with each decomp involving splitting an array or input in 1/2 meaning O(log n) -> n * log n
    - Space Complexity: Array is split into two -> merge subarrays with temporary array
        -  Temporary array has same size as input array
    - Overall SC: O(n)

- Merge Sort Implementation
    1. Implement function to merge 2 sorted arrays `function merge`
    2. given 2 sorted arrays, helper function creates new array that is sorted consisting of all elements in the 2 input arrays `merge(mergeSort(left), mergeSort(right))`
        * Helper function runs in tc and sc of O(n+m)
        * Does not modify parameters passed as input
        * n being the size of 1st array and m being the size of the 2nd array

    - `function merge`
        1. create empty array and look at smallest values in each input array
        2. while there are values we haven't looked at
        3. if values in 1st array is smaller than value in 2nd push value from 1st array into results and move to next value in next array
        4. if value in 1st array is larger than 2nd array, perform opposite of 3
        5. Once one array is done, push other values from other array

            - Time Complexity: O(n log n)
            - Space Complexity: O(n)

### __Quick Sort__
[QuickSort Implementation](QuickSort.js)
- Good for reversed and random data, few unique as well as large sets
    - exploits the fact that arrays of 0, 1 elements are sorted like in Merge Sort
    - One element is selected at the `pivot` and find index where pivot ends up in sorted array -> One pivot is appropriately positioned, quick sort is applied on either pivot side
        - Time Complexity: O(n log n)
        - Space Complexity: Depends on specific implementation, but generally is O(log n) space

- Quick Sort Implementation
    1. implement fxn for arrangement of elements in array on either side of designated pviot `function pivot(arr, start=0, end=arr.length-1)`
    2. given an array, helper function designates element as pivot `pivotIndex=pivot(arr, left, right)`
    3. rearrange elements in array so values < pivot are moved to left, values > pivot are moved to right
    ```js
    quicksort(arr, left, pivotIndex-1)
    quicksort(arr, pivotIndex-1, right)
    ```
    4. helper performs swapping in place and on completion helper fxn returns pivot index

### __Radix Sort__
- ...
    - Time Complexity:
    - Space Complexity:

## Topal Sorting Site
[Visual Sorting Site](https://www.toptal.com/developers/sorting-algorithms)
* Crux
    - Choice of sorting algorithm depends on application with the ideal sorting algo generally having these properties:
        - Stable [ equal keys are not reordered ], operates in place and thus requires O(1) space, worst case it has O(n log n) key comparisons, worst case O(n) swaps and speeds up to O(n) when data is near being sorted/few unique keys

* Scenario Sorting: (Most to Least Efficient Performance)
    - Randomly Sorted Array:
        - Heap -> __Quick__ -> __Merge__ -> Shell ->  (worst case behavior) __Bubble__ -> __Insertion__-> __Selection__
        - Bubble, Insertion, and Selection being generally O(N^2) are last
    - Nearly Sorted Array (common practice)
        - __Insertion__ -> __Bubble__ -> Shell -> [__Merge__, Heap, __Quick__] (generally finished around the same time) -> __Selection__
        - Insertion is clear winner for nearly sorted, Bubble is fast but insertion has a slower overhead, shell is based on insertion sort
        - Insertion is O(N^2) but adapts to O(N) when data is close to being sorted
    - Reversed Array:
        - Heap -> __Quick__ -> Shell -> __Merge__ -> __Bubble__ -> __Insertion__ -> __Selection__
        - Merge exhibits worst case behavior with Reversed Array
    - Few Unique Array (common):
        -  __Insertion__ -> __Shell__ (close to insertion in regards to performance) -> __Quick__ -> Heap -> __Merge__ -> __Bubble__ -> __Selection__
        - Sorting an array that has a small number of unique keys, one would want an algo that adapts to O(N) when unique keys are constant
        - Quicksort exhibits worst case O(N^2) if there isn't 3 way partitioning
            - A suggestion was made to implement 3 pivots that are <, = , > pivot thus making quick adapt to O(N) worst case
