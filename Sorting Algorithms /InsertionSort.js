/*
    Insertion Sort
    Builds sort by creating larger left half that is always sorted
    It is good for few unique values and nearly sorted
*/

/*
    tc: 0(N^2) worst case if list is entirely reversed,
    if already sorted best case is O(n)
    sc: O(1) due constant # of vars
*/

const swap = (arr, idx1, idx2) => {
    return [arr[idx1], arr[idx2]] = [arr[idx2], arr[idx1]];
}

function insertionSort(arr) {
    var len = arr.length
    // i starts from second element of array
    for (let i = 1; i < len; i++) {
        // compare second element to one prior, swap if req
        for (let j = i - 1; j >= 0 && arr[j] > arr[i]; j--) {
            if (arr[j] > arr[j + 1]) {
                swap(arr, j + 1, j);
            }
            // stop backward iteration if previous element is greater
            else break;
        }
    }
    return arr;
}

console.log(insertionSort([34, 0, 2, 10, 17, 19, 22]))

/*
    continue to next elements, if in incorrect order iterate through
    sorted portion to place element correctly
    repeat until sorted array
*/
