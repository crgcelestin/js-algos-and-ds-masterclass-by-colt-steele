/*
    Pivot pseudocde:
    accepts three arguments - start, end index, arr
    grab pivot from start of array
    store current pivot index in var (track of where pivot is to end)
    loop through array from start till end,
    (for var i=start+1;i<=end;i++)
        if pivot > current element (if pivot>arr[i])
      increment pivot index var (swapIdx++), swap
        current ele (i) with ele at pivot index (swapIdx) swap(arr, swapIdx,i)
    swap starting element with pivot index [ swap(arr, start, swapIdx)
    return swapIdx;
*/

function pivot(arr, start = 0, end = arr.length - 1) {
    const swap = (arr, idx1, idx2) => {
        return [arr[idx1], arr[idx2]] = [arr[idx2], arr[idx1]]
    }
    // simplicity = choose pivot to be first element
    let pivot = arr[start]
    let swapIdx = start
    for (let i = start + 1; i <= end; i++) {
        if (pivot > arr[i]) {
            swapIdx++;
            swap(arr, swapIdx, i)
        }
    }
    swap(arr, start, swapIdx)
    return swapIdx
}

/*
    QuickSort Psuedcode
    (1) call pivot helper on array
    (2) when helper returns updated pivot index
            recurisvely call pivot helper on subarray to left of index
            and subarray to right of index
    (3) base case - check if left and right are equal or left is less than right
    (4) Conisder subarray with < 2 elements
*/

function quickSort(arr, left = 0, right = arr.length - 1) {
    if (left === right || left < right) {
        let pivotIndex = pivot(arr, left, right)
        quickSort(arr, left, pivotIndex - 1)
        quickSort(arr, pivotIndex + 1, right)
    }
    return arr;
}
