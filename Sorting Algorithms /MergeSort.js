/*
    (1) create empty array and look at smallest values in each input array
    (2) while there are still values we haven't looked at
    (3) if value in 1st array is smaller than value in 2nd array
        - push value from 1st array into results and move to next value in 1st array
        - else push value from 2nd array and move to next value in 2nd array
    (4) Once one array is exhausted, push remaining values from other array
*/
function merge(arr1, arr2, results = []) {
    let i = 0;
    let j = 0;
    while (i < arr1.length && j < arr2.length) {
        if (arr1[i] < arr2[j]) {
            results.push(arr1[i])
            i++
        } else {
            results.push(arr2[j])
            j++
        }
    }
    results.push(...arr1.slice(i), ...arr2.slice(j));
    return results
}

/*
    (1) return array if length is less than or equal to 1
    (2) break array into halves until base case (using Math.floor and .slice)
    (3) with last return line, smaller sorted arrays will merge with each other until its back to full length
    (3) once array is merge return sorted array
*/
const mergeSort = (arr) => {
    if (arr.length <= 1) return arr;
    const middle = Math.floor(arr.length / 2)
    const left = arr.slice(0, middle)
    const right = arr.slice(middle)
    return merge(mergeSort(left), mergeSort(right))
}
console.log(mergeSort([4, 3, 2, 1]))
