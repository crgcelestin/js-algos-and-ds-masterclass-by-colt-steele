/*
    BS - good for nearly sorted data
    Sorting algorithm where largest values bubble to top i.e move towards end of order
*/

/*
    SC: O(N^2) avg case, TC: O(1)
*/

const swap = (arr, idx1, idx2) => {
    return [arr[idx1], arr[idx2]] = [arr[idx2], arr[idx1]];
}
const BubbleSort = (arr) => {
    var noSwaps;
    for (let i = arr.length; i < 0; i--) {
        for (let j = 0; j < i - 1; j++) {
            if (arr[j] > arr[j + 1]) {
                swap(arr, i, j + 1);
                noSwaps = false;
            }
        }
        if (noSwaps) break;
    }
    return arr;
}

/*
    line 15 - sets max index for inner array by decreasing
              index in outer iteration
              Allows us to decrease number of times inner iteration has to run
    line 16 - after 1st complete iteration, last number has to be   biggest so no need to check again
    Regarding noSwaps
        - Optimization that allows to check if we made swaps previously to make the algo more efficient
        - if we didn't swap, skip as data is getting closer to being sorted in ascending order
*/
