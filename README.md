# JS Algos and DS Masterclass by Colt Steele

Notes and Problems solved during the Udemy JS Algo and Data Structures Masterclass course

## Topics Include
* Big O
* Analyze the Performance of Arrays and Object
* Problem Solving Approaches, Optional Challenges
* Recursion, Problem Sets, Challenging Problems
* Searching Algorithms
* Sorting Algorithms 
  - Bubble Sort
  - Selection Sort
  - Insertion Sort
  - Comparison: Bubble v Selection v Insertion
  - Merge Sort
  - Quick Sort
  - Radix Sort
* Data Structures
  - Singly Linked Lists
  - Doubly Linked Lists
  - Stacks + Queues
  - Binary Search Trees, Tree Traversal
  - Binary Heaps
  - Hash Tables
  - Graphs, Graph Traversal
* Dijkstra's Algo
* Dynamic Programming
* Wild West


## Course
<https://www.udemy.com/course/js-algorithms-and-data-structures-masterclass/>