# DP = Dynamic Programming
## Intro
- DP Definition: method for solving complex problem by breaking it into a collection of simpler subproblem
    * solve each of these subproblems once and store solutions

## Overlapping Sub Problems
- if problem is overlapping subproblem, break it into smaller sub units that are able to be reused
    * relates to recursion-optimal problems like Fibonacci
    * mergesort with dp allows for mitigation of overhead, tc
## Optimal SubStructure
- optimal structure can be gained from optimal solutions of subproblems (shortest path constructed from shortest intermediate paths)

## Recursive Solution + TC
```js
function fib(n){
    if(n<=2) return 1;
    return fib(n-1)+fib(n-2);
}
console.log(fib(6))
/*
    Big O 2^n - larger #s require increased steps towards exponential
    Not remembering previous calcs of Fib(n) -> continue to recalculate fib calcs that are not base calcs
*/
```
## Memoization
- Produces an O(n) tc with array growing linear and 3 of steps growing at same rate
    * [Memo alt](Memo.js)

## Tabulation
- Working with a bottom up approach (start with what we know and work towards unknown)
```js
//tab version
function fib(n, fibNums=[0,1,1]){
	for(var i=3;i<=n;i++){
		fibNums[i]=fibNums[i-1]+fibnums[i-2];
	}
	return fibNums[n];
}//TC is also O(n) with sc being better than memo
```
