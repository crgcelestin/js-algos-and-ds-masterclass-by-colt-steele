// memo-ized solution
function fib(n, memo = []) {
    if (memo[n] !== undefined) return memo[n];
    if (n <= 2) return 1;
    let res = fib(n - 1, memo) + fib(n - 2, memo);
    memo[n] = res
    return memo[n]
}

//alt1 with n<=2 base case, i.e. base case calcs are not stored
function fib(n, memo = []) {
    if (memo[n] !== undefined) return memo[n];
    if (n <= 2) return 1;
    memo[n] = fib(n - 1, memo) + fib(n - 2, memo);
    return memo[n]
}

console.log(fib(100))

//alt2 (involves initialization of array with first 3 integers in memo array
function fib(n, memo = [0, 1, 1]) {
    if (memo[n] !== undefined) return memo[n];
    memo[n] = fib(n - 1, memo) + fib(n - 2, memo);
    return memo[n]
}
// much quicker response
console.log(fib(100))

// could also use object storage, predefine base case
