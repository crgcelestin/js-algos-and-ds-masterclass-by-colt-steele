/*
    DS w/ head, tail, length property
    LL has nodes
    Each node has value, pointer to another node/null
    If at end, current node refs null
    nodes are in sequential order (only stairs)
    Required to have a head
*/

/*
    each node has piece of data (value)
    and reference to next node (next/link)
*/

class Node {
    constructor(value) {
        this.value = value
        this.next = null;
    }
}

class LinkedList {
    constructor() {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }
    /*
        Pushing pseducode - push nodes into LL from node 0
        (1) fxn accepts value
        (2) create new node w/ value passed
        (3) no head prop on list, seat head and tail to new
        created node
        (4) else set next property on tail to be new node
        and set tail property on list to be new created node
        (5) increment length by 1
    */
    push(val) {
        var newNode = new Node(val)
        if (!this.head) {
            this.head = newNode
            this.tail = this.head
        } else {
            this.tail.next = newNode
            this.tail = newNode;
        }
        this.length += 1
        return this
    }
    //LL traversal 'traverse LL through nodes'
    traverse() {
        //store front of LL
        var current = this.head;
        //while there is still a node in LL
        while (current) {
            //print current's value
            console.log(current.value)
            //traverse to next node
            current = current.next;
        }
    }
    /*
        Popping - remove node from end of LL
            Psuedocode
            (1) No nodes in list return undefined
            (2) loop through list until reach tail
            (3) set tail to be 2nd to last node
            (4) set next property of 2nd to last node to be null
            (5) Decrement list length by 1
            (6) Return value of node removed
    */
    //Correct LL pop
    //delete node at end i.e 'pop it off'
    pop() {
        if (!this.head) return null;
        var current = this.head;
        var newtail = current;
        if (this.length > 1) {
            while (current.next) {
                newtail = current;
                current = current.next;
            }
            this.tail = newtail;
            this.tail.next = null;
            this.length -= 1
            return current
        }
        /*
        catch edge case of 1 item,
        tail and head should be the same if <= 1 item
        */
        if (this.head === this.tail) {
            this.length -= 1
            current = this.tail
            this.tail = null
            this.head = null
            return current
        }
        return null;
    }
    /*
     list1
    SinglyLinkedList {
    head: Node { value: 12, next: null },
    tail: Node { value: 12, next: null },
    length: 0
    }
     list1.push('value')
    undefined
     list1
    SinglyLinkedList {
    head: Node { value: 12, next: Node { value: 'value', next: null } },
    tail: Node { value: 'value', next: null },
    length: 0
    }
    */
    deleteMiddleNode(head) {
        let tempHead = this.head
        let slow = tempHead;
        let fast = head?.next;
        while (fast) {
            slow = slow.next;
            fast = fast.next?.next;
        }
        slow.next = slow.next ? slow.next.next : null;
        this.length -= 1
        return tempHead.next;
    }
    /*
        remove first node in LL
        (1) no nodes return undefined
        (2) store current head property in var
        (3) set head prop to current head's next propr
        (4) decrement length by 1
        (5) return value of removed node
        ! make sure to check base case of LL methods !
            for shifting, unshifting, pop
                these methods need to reach an end state
                where LL looks like
                SinglyLinkedList { head: null, tail: null, length: 0 }
    */
    shift() {
        if (!this.length) return null;
        var current = this.head;
        this.head = this.head.next;
        this.length -= 1;
        if (this.length === 0) {
            this.tail = null;
        }
        return current
    }
    /*
        add new node to start of LL
        (1) fxn accepts value
        (2) create new node using value passed to fxn
        (3) no head property on list, set head
        and tail to be new node
        (4) else set new create node's next property
        to be current head prop on list
        (5) set head prop on list to be new node
        (6) increment list length by 1
        (7) return linked list
    */
    unshift(val) {
        var newNode = new Node(val)
        if (!this.head) {
            //runs if list is empty
            this.head = newNode
            this.tail = this.head
        } else {
            //runs if there are nodes already
            newNode.next = this.head
            this.head = newNode
        }
        this.length += 1
        return this
    }
    /*
        get node at specified index
        (1) function accepts index
        (2) if position > length, return null
        (3) position<0, convert negative to correct position
            ex: -1 convert to last by adding to length, and return that summed value as the position
        (4) loop through list until index, return node at index
    */
    get(position) {
        if (position > this.length || position <= -this.length) return null;
        if (position < 0) position += this.length;
        var current = this.head;
        var count = 0;
        while (count < position) {
            current = current.next;
            count += 1;
        }
        return current;
    }
    search(val) {
        let curr = this.head
        for (let i = 0; i < this.length; i++) {
            if (curr.value === val) return i;
            else curr = curr.next;
        }
        return 'not found'
    }
    /*
        set value of node at x position
        (1) fxn accepts val, index
        (2) use get fxn to find specific node
        (3) if node not found, return false
        (4) node is found, set value of node
            to val passed in and return true
    */
    set(position, val) {
        var node = this.get(position)
        if (node) {
            node.value = val
            return true
        }
        return false
    }
    /*
        assuming start at 0th index
        insert node with node.value=val at specified position (1st,2nd,...x)
        on LinkedList
        (1) store node to be inserted in LL with provided value
        (2) if the position specified is === LL.length, push it to end
        (3) else if position specified is at start, then unshift value to LL
        (4) else if position is nonzero and not at end
            (5) user ternary, if position >0 then subtract 1, else let .get return correct index of negative position
            (6) for insertion method, store nodes following node at that position previously
            (7) set the next pointer of current node, as newNode
            (8) set the next pointer of newNode as previous
            (9) increment length property
            (10) return true that insertion was successful
        if successful, test insert() is correct using .remove()
    */
    insert(position, val) {
        var newNode = new Node(val)
        if (position - 1 === this.length) {
            return !!this.push(val)
        } else if (position === 0) {
            return !!this.unshift(val)
        } else if (position > this.length) {
            return false;
        } else {
            var lastNode = position > 0 ? this.get(position - 1) : this.get(position)
            var temp = lastNode.next
            lastNode.next = newNode
            newNode.next = temp
            this.length += 1
            return true
        }
    }

    /*
        removal is remove node at specified position
        similar to insert() but the reverse
        (1) store node at specified position
        (2) if position is = length of LL, pop and return value of popped node
        (3) else if position is = 0, use shift() which removes 0th node then return that node's value
        (4) convert to correct position depending on if negative or positive (greater or less than 0)
         (5) store value of node to be removed
         (6) if next node is actually last i.e -1, then pop and return its value
         (7) if non negative and greater than 0, store node previous in lastNode var and then set next of that node to the node that is next after the desired node to be removed
         (8) return value of that removed node
    */
    remove(position) {
        var posNode = position > 0 ? this.get(position - 1) : this.get(position)
        this.length -= 1
        if (position - 1 === this.length) {
            !!this.pop()
            return posNode.value
        } else if (position - 1 === 0) {
            !!this.shift()
            return posNode.value
        } else if (position > this.length) {
            return false;
        } else {
            var posValue = posNode.value
            if (posNode.next === null) {
                !!this.pop()
                return lastNode.value
            } else {
                posNode.next = posNode.next.next
                return posValue
            }
        }
    }
    /*
        reverse linked list nodes
        (1) store current head in var node
        (2) set head to tail and set tail to the head's previous data
        (3) initialize a next variable, set a prev variable to null (assuming we start from first node)
        (4) iterate through lL using C style loop
            (5) store node after current in next var
            (6) (starting from start of LL) new tail which is now the head points to null as we start LL
                *in general*
                next pointer of current node points to previous
            (7) assign previous node to previous variable
            (8) to continue iterating and reversing, set current node to be equal to the node the next variable is on
    */
    reverse() {
        if (!this.head) return null;
        var node = this.head;
        if (!node.next || !node) return node;
        this.head = this.tail;
        this.tail = node;
        var next;
        var prev = null;
        //traverse through LL
        for (let i = 0; i < this.length; i++) {
            //store node after current node in next var
            next = node.next
            //update next of current node to now point to previous node (in case of start it is null)
            node.next = prev
            //assign previous node to prev variable
            prev = node;
            // assign node var to now be the node next is on
            // restart loop
            node = next;
        }
        return this
    }
}

module.exports = LinkedList;
