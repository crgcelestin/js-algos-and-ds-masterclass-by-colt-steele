## Getting Started
### 1. Update the path to your data structure
Open the test that corresponds to the data structure you're studying. (For instance, if you're working on Section 25: Hash Tables, open tests/hashtable.js) On line 2, update the file path.

```
// TO DO: Replace with your own path
const HashTable = require('./PATH/TO/HASHTABLE');
```

### 2. Export your data structure
At the bottom of your own file, export the data structure you coded.
```
module.exports = HashTable;
```

### 3. Run the test
```
Example:
node hashtable.js
(while in the hashTable directory in UnitTesting Folder)
```

__MISSING__
- Graphs Test [ ]
- Stacks, Queues Test [ ]
